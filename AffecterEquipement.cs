﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TestGSB1
{
    public partial class AffecterEquipement : Form
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///     VARIABLES PRIVEES ET DECLARATION
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private String listeEquipementSQL = "SELECT eq.id as 'ID', ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie (an)', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is null";
        private String listeEquipementAffectesSQL = "SELECT eq.id, ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie', eq.date_assignation as 'Assigné le', ut.prenom+' '+ut.nom as 'Assigné à', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is not null";
        private String id_equipement;
        private String idUser;

        public AffecterEquipement(String equip_select, String id_utilisateur)
        {
            InitializeComponent();
            idUser = id_utilisateur;
            listView1.Items.Add(equip_select);
        }

        public String Id_equipement
        {
            get { return id_equipement; }
            set { id_equipement = value; }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents [AffecterEquipement] - Clic Bouton Affecter 
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            String id_utilisateurs = ((ComboBoxItem)cbVisiteurAffect.SelectedItem).HiddenValue;

            String upd_affectSQL = "UPDATE equipements SET id_utilisateurs = @id_utilisateurs, date_assignation = @date_assignation WHERE id = @id_equipement";
            SqlCommand upd_affectComm = new SqlCommand(upd_affectSQL, conn);
            upd_affectComm.Parameters.AddWithValue("@id_utilisateurs", id_utilisateurs);
            upd_affectComm.Parameters.AddWithValue("@date_assignation", dateTimePicker1.Value);
            upd_affectComm.Parameters.AddWithValue("@id_equipement", this.Id_equipement);

            conn.Open();
            int rowsAffected = upd_affectComm.ExecuteNonQuery();
            this.Hide();
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            gp.GetData(listeEquipementSQL, connectionString);
            gp.GetData2(listeEquipementAffectesSQL, connectionString);

        }

        private void AffecterEquipement_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String types_equipSQL = "SELECT id, nom+' '+prenom as nom_prenom FROM utilisateurs where id_profils = 1";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbVisiteurAffect.Items.Add(new ComboBoxItem(dr["nom_prenom"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();
        }

        private void AffecterEquipement_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            gp.Show();
        }
    }
}
