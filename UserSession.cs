﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGSB1
{
    class UserSession
    {
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static String idUserSession;
        private String idUser;
        private String firstname;
        private String lastname;
        private String adresse_mail;
        private String avatar;
        private String idProfil;

        public UserSession()
        {

        }

        public UserSession(String idUser)
        {
            idUserSession = idUser;

            SqlConnection conn = new SqlConnection(connectionString);

            String id_utilisateur = idUser;

            String upd_affectSQL = "SELECT id as id, nom as lastname, prenom as firstname, adresse_mail as adresse_mail, avatar as avatar, id_profils as idProfil FROM utilisateurs WHERE id = @id_utilisateur";
            SqlCommand sel_userComm = new SqlCommand(upd_affectSQL, conn);

            sel_userComm.Parameters.AddWithValue("@id_utilisateur", id_utilisateur);

            conn.Open();

            SqlDataReader dr = sel_userComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    this.idUser = dr["id"].ToString();
                    this.lastname = dr["lastname"].ToString();
                    this.firstname = dr["firstname"].ToString();
                    this.adresse_mail = dr["adresse_mail"].ToString();
                    this.avatar = dr["avatar"].ToString();
                    this.idProfil = dr["idProfil"].ToString();
                }
            }
            
        }

        public String IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }

        public String Firstname
        {
            get { return firstname; }
            set { firstname = value; }
        }

        public String Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }

        public String Adresse_mail
        {
            get { return adresse_mail; }
            set { adresse_mail = value; }
        }

        public String Avatar
        {
            get { return avatar; }
            set { avatar = value; }
        }

        public String IdProfil
        {
            get { return idProfil; }
            set { idProfil = value; }
        }

        public void Logout()
        {
            this.IdUser = "";
            this.Lastname = "";
            this.Firstname = "";
            this.Adresse_mail = "";
            this.Avatar = "";
            this.IdProfil = "";
        }

    }
}
