﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace TestGSB1
{
    public partial class Connexion : Form
    {
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public Connexion()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String isUserSQL = "SELECT id as id, adresse_mail as adresse_mail, password as password FROM utilisateurs";
            SqlCommand isUserComm = new SqlCommand(isUserSQL, conn);
            SqlDataReader dr = isUserComm.ExecuteReader();
            bool isUser = false;
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    // Vérification avec hashage mot de passe
                    if ((dr["adresse_mail"].ToString() == textBox1.Text) && (dr["password"].ToString() == ConvertMD5(textBox2.Text)))
                    {                      
                        UserSession User = new UserSession(dr["id"].ToString());
                        GestionParc frm = new GestionParc(User.IdUser, User.Firstname+" "+User.Lastname,User.Adresse_mail,User.IdProfil,User.Avatar);
                        frm.Show();
                        this.Hide();
                        this.Cursor = Cursors.Default;
                        isUser = true;
                        break;
                    }
                    else
                    {
                        isUser = false;                   
                    }
                }                
            }
            if (!isUser)
            {
                MessageBox.Show("Les identifiants entrés sont incorrects. Veuillez réessayer.", "Connexion impossible", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
            }
        }

        private void Connexion_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Connexion_Load(object sender, EventArgs e)
        {

        }

        public string ConvertMD5(string value)
        {
            MD5 md = MD5.Create();
            byte[] data = md.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
