﻿namespace TestGSB1
{
    partial class DemandeIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DemandeIncident));
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cbDmandeurIncident = new System.Windows.Forms.ComboBox();
            this.cbEquipementIncident = new System.Windows.Forms.ComboBox();
            this.tbObjet = new System.Windows.Forms.TextBox();
            this.tbNiveauIncident = new System.Windows.Forms.TrackBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tbNiveauIncident)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Niveau :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 158);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Objet : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Equipement :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Demandeur :";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(174, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(216, 22);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // cbDmandeurIncident
            // 
            this.cbDmandeurIncident.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDmandeurIncident.FormattingEnabled = true;
            this.cbDmandeurIncident.Location = new System.Drawing.Point(174, 70);
            this.cbDmandeurIncident.Name = "cbDmandeurIncident";
            this.cbDmandeurIncident.Size = new System.Drawing.Size(216, 24);
            this.cbDmandeurIncident.TabIndex = 10;
            this.cbDmandeurIncident.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // cbEquipementIncident
            // 
            this.cbEquipementIncident.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEquipementIncident.FormattingEnabled = true;
            this.cbEquipementIncident.Location = new System.Drawing.Point(174, 111);
            this.cbEquipementIncident.Name = "cbEquipementIncident";
            this.cbEquipementIncident.Size = new System.Drawing.Size(216, 24);
            this.cbEquipementIncident.TabIndex = 11;
            // 
            // tbObjet
            // 
            this.tbObjet.Location = new System.Drawing.Point(174, 155);
            this.tbObjet.Name = "tbObjet";
            this.tbObjet.Size = new System.Drawing.Size(216, 22);
            this.tbObjet.TabIndex = 12;
            // 
            // tbNiveauIncident
            // 
            this.tbNiveauIncident.Location = new System.Drawing.Point(197, 272);
            this.tbNiveauIncident.Margin = new System.Windows.Forms.Padding(3, 1, 3, 1);
            this.tbNiveauIncident.Maximum = 5;
            this.tbNiveauIncident.Minimum = 2;
            this.tbNiveauIncident.Name = "tbNiveauIncident";
            this.tbNiveauIncident.Size = new System.Drawing.Size(232, 56);
            this.tbNiveauIncident.TabIndex = 13;
            this.tbNiveauIncident.Value = 2;
            this.tbNiveauIncident.MouseHover += new System.EventHandler(this.trackBar1_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbObjet);
            this.groupBox1.Controls.Add(this.cbDmandeurIncident);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cbEquipementIncident);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(23, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(435, 268);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informations sur l\'incident";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(80, 356);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(311, 39);
            this.button1.TabIndex = 17;
            this.button1.Text = "Nouvel incident";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(-11, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(514, 45);
            this.panel1.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(92, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(310, 28);
            this.label12.TabIndex = 19;
            this.label12.Text = "Nouvelle demande d\'incident";
            // 
            // DemandeIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(481, 407);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbNiveauIncident);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DemandeIncident";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Demande d\'incident";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DemandeIncident_FormClosed);
            this.Load += new System.EventHandler(this.DemandeIncident_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbNiveauIncident)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.ComboBox cbDmandeurIncident;
        private System.Windows.Forms.ComboBox cbEquipementIncident;
        private System.Windows.Forms.TextBox tbObjet;
        private System.Windows.Forms.TrackBar tbNiveauIncident;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label12;
    }
}