﻿namespace TestGSB1
{
    partial class GestionParc
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GestionParc));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.gestionParcT = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.btnExportPDF = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvEquipAffecte = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvEquipNonAffecte = new System.Windows.Forms.DataGridView();
            this.incidentsT = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.gbCacheDetails = new System.Windows.Forms.GroupBox();
            this.idIncidentSelected = new System.Windows.Forms.Label();
            this.label_no_resol = new System.Windows.Forms.Label();
            this.btnResol_incident = new System.Windows.Forms.Button();
            this.date_interv = new System.Windows.Forms.Label();
            this.technicien_incident = new System.Windows.Forms.Label();
            this.duree_interv = new System.Windows.Forms.Label();
            this.labDuree_interv = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.labSolution_incid = new System.Windows.Forms.Label();
            this.labSolution_incident = new System.Windows.Forms.TextBox();
            this.labDate_interv = new System.Windows.Forms.Label();
            this.labTechn_incid = new System.Windows.Forms.Label();
            this.objet_incident = new System.Windows.Forms.Label();
            this.demandeur_incident = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.duree_intervention = new System.Windows.Forms.Label();
            this.date_intervention = new System.Windows.Forms.Label();
            this.equipement = new System.Windows.Forms.LinkLabel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.date_demande = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvIncidents = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.btnFiltreNiveau = new System.Windows.Forms.Button();
            this.cbFiltreStatut = new System.Windows.Forms.ComboBox();
            this.cbfiltreNiveau = new System.Windows.Forms.ComboBox();
            this.btnFiltreStatut = new System.Windows.Forms.Button();
            this.celluleAssistanceT = new System.Windows.Forms.TabPage();
            this.btnValiderStatsGraph = new System.Windows.Forms.Button();
            this.cbYearChart = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cbMonthChart = new System.Windows.Forms.ComboBox();
            this.chartNbIncidentMonth = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.nbIncidentsResolusMonth = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.nbEquipAffectesMonth = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.dureeMoyResol = new System.Windows.Forms.Label();
            this.nbIncidentsResolus = new System.Windows.Forms.Label();
            this.nbIncidents = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.nbEquipementsAffectes = new System.Windows.Forms.Label();
            this.nbEquipements = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.nbVisiteurs = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.configurationA = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btnSaveUniteComposants = new System.Windows.Forms.Button();
            this.tbUniteComposA = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btnSaveStatutIncidents = new System.Windows.Forms.Button();
            this.tbStatutIncidA = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnSaveMarques = new System.Windows.Forms.Button();
            this.tbMarque = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btnSaveProfils = new System.Windows.Forms.Button();
            this.tbProfilA = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnSaveNiveauIncidents = new System.Windows.Forms.Button();
            this.tbNivIncidA = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnSaveEtatEquipements = new System.Windows.Forms.Button();
            this.tbEtatEquipA = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnSaveTypeComposants = new System.Windows.Forms.Button();
            this.tbTypeComposA = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnSaveLogicielEquipements = new System.Windows.Forms.Button();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnSaveTypeEquipements = new System.Windows.Forms.Button();
            this.tbTypeEquipements = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbMailFournissA = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.tbTelFournissA = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.btnSaveFournisseurs = new System.Windows.Forms.Button();
            this.tbAdrFournissA = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbNomFournissA = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSaveUtilisateur = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.tbPasswordA = new System.Windows.Forms.TextBox();
            this.tbMailA = new System.Windows.Forms.TextBox();
            this.tbPrenomA = new System.Windows.Forms.TextBox();
            this.tbNomA = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbProfils = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.celluleAssistanceV = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lbEquipements = new System.Windows.Forms.ListBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label51 = new System.Windows.Forms.Label();
            this.labNbEquipV = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.labNbIncidAttenteV = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.labNbIncidResolV = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.labNbDemandesIncidV = new System.Windows.Forms.Label();
            this.incidentsV = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.labDateResolIncidV = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.labTechnicIncidV = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.tbResolIncidV = new System.Windows.Forms.TextBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.labStatutIncidV = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.labIdIncidV = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.labEquipIncidV = new System.Windows.Forms.Label();
            this.labDemandeurIncidV = new System.Windows.Forms.Label();
            this.labObjIncidV = new System.Windows.Forms.Label();
            this.labDateDemandeIncidV = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.dgvSuiviIncidentsV = new System.Windows.Forms.DataGridView();
            this.equipementV = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.clbLogiciels = new System.Windows.Forms.CheckedListBox();
            this.lvCarac = new System.Windows.Forms.ListView();
            this.label53 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.dtpDateAssignation = new System.Windows.Forms.DateTimePicker();
            this.cbMarque = new System.Windows.Forms.ComboBox();
            this.cbTypeEquip = new System.Windows.Forms.ComboBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.cbEtat = new System.Windows.Forms.ComboBox();
            this.cbVisiteur = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.nupGarantie = new System.Windows.Forms.NumericUpDown();
            this.label68 = new System.Windows.Forms.Label();
            this.dtpDateAchat = new System.Windows.Forms.DateTimePicker();
            this.label69 = new System.Windows.Forms.Label();
            this.LabIdEquipement = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.dgvSuiviEquipementsV = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.gestionParcT.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipAffecte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipNonAffecte)).BeginInit();
            this.incidentsT.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncidents)).BeginInit();
            this.panel3.SuspendLayout();
            this.celluleAssistanceT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartNbIncidentMonth)).BeginInit();
            this.panel4.SuspendLayout();
            this.configurationA.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.celluleAssistanceV.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.incidentsV.SuspendLayout();
            this.panel7.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuiviIncidentsV)).BeginInit();
            this.equipementV.SuspendLayout();
            this.panel8.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupGarantie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuiviEquipementsV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.gestionParcT);
            this.tabControl1.Controls.Add(this.incidentsT);
            this.tabControl1.Controls.Add(this.celluleAssistanceT);
            this.tabControl1.Controls.Add(this.configurationA);
            this.tabControl1.Controls.Add(this.celluleAssistanceV);
            this.tabControl1.Controls.Add(this.incidentsV);
            this.tabControl1.Controls.Add(this.equipementV);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1556, 748);
            this.tabControl1.TabIndex = 0;
            // 
            // gestionParcT
            // 
            this.gestionParcT.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.gestionParcT.Controls.Add(this.panel1);
            this.gestionParcT.Controls.Add(this.dgvEquipAffecte);
            this.gestionParcT.Controls.Add(this.label6);
            this.gestionParcT.Controls.Add(this.dgvEquipNonAffecte);
            this.gestionParcT.Location = new System.Drawing.Point(4, 25);
            this.gestionParcT.Name = "gestionParcT";
            this.gestionParcT.Padding = new System.Windows.Forms.Padding(3);
            this.gestionParcT.Size = new System.Drawing.Size(1548, 719);
            this.gestionParcT.TabIndex = 0;
            this.gestionParcT.Text = "Gestion du parc";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.btnExportPDF);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label64);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(-14, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1587, 61);
            this.panel1.TabIndex = 38;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(307, 23);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "reload";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnExportPDF
            // 
            this.btnExportPDF.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnExportPDF.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.btnExportPDF.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnExportPDF.Location = new System.Drawing.Point(1395, 12);
            this.btnExportPDF.Name = "btnExportPDF";
            this.btnExportPDF.Size = new System.Drawing.Size(142, 41);
            this.btnExportPDF.TabIndex = 21;
            this.btnExportPDF.Text = "Export PDF";
            this.btnExportPDF.UseVisualStyleBackColor = false;
            this.btnExportPDF.Click += new System.EventHandler(this.btnExportPDF_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(743, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 28);
            this.label1.TabIndex = 20;
            this.label1.Text = "Equipements affectés";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label64.Location = new System.Drawing.Point(24, 19);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(242, 28);
            this.label64.TabIndex = 19;
            this.label64.Text = "Liste des équipements";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.button1.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.button1.Location = new System.Drawing.Point(480, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(238, 41);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ajouter un équipement";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvEquipAffecte
            // 
            this.dgvEquipAffecte.AllowUserToAddRows = false;
            this.dgvEquipAffecte.AllowUserToDeleteRows = false;
            this.dgvEquipAffecte.AllowUserToResizeColumns = false;
            this.dgvEquipAffecte.AllowUserToResizeRows = false;
            this.dgvEquipAffecte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEquipAffecte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEquipAffecte.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvEquipAffecte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquipAffecte.Location = new System.Drawing.Point(723, 67);
            this.dgvEquipAffecte.MultiSelect = false;
            this.dgvEquipAffecte.Name = "dgvEquipAffecte";
            this.dgvEquipAffecte.ReadOnly = true;
            this.dgvEquipAffecte.RowHeadersVisible = false;
            this.dgvEquipAffecte.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEquipAffecte.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEquipAffecte.RowTemplate.Height = 24;
            this.dgvEquipAffecte.RowTemplate.ReadOnly = true;
            this.dgvEquipAffecte.ShowEditingIcon = false;
            this.dgvEquipAffecte.Size = new System.Drawing.Size(800, 646);
            this.dgvEquipAffecte.TabIndex = 4;
            this.dgvEquipAffecte.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEquipAffecte_CellClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(290, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 17);
            this.label6.TabIndex = 3;
            // 
            // dgvEquipNonAffecte
            // 
            this.dgvEquipNonAffecte.AllowUserToAddRows = false;
            this.dgvEquipNonAffecte.AllowUserToDeleteRows = false;
            this.dgvEquipNonAffecte.AllowUserToResizeColumns = false;
            this.dgvEquipNonAffecte.AllowUserToResizeRows = false;
            this.dgvEquipNonAffecte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvEquipNonAffecte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEquipNonAffecte.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvEquipNonAffecte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquipNonAffecte.Location = new System.Drawing.Point(8, 67);
            this.dgvEquipNonAffecte.MultiSelect = false;
            this.dgvEquipNonAffecte.Name = "dgvEquipNonAffecte";
            this.dgvEquipNonAffecte.ReadOnly = true;
            this.dgvEquipNonAffecte.RowHeadersVisible = false;
            this.dgvEquipNonAffecte.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEquipNonAffecte.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEquipNonAffecte.RowTemplate.Height = 24;
            this.dgvEquipNonAffecte.RowTemplate.ReadOnly = true;
            this.dgvEquipNonAffecte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEquipNonAffecte.ShowEditingIcon = false;
            this.dgvEquipNonAffecte.Size = new System.Drawing.Size(696, 646);
            this.dgvEquipNonAffecte.TabIndex = 0;
            this.dgvEquipNonAffecte.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dgvEquipNonAffecte.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // incidentsT
            // 
            this.incidentsT.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.incidentsT.Controls.Add(this.panel2);
            this.incidentsT.Controls.Add(this.groupBox4);
            this.incidentsT.Controls.Add(this.dgvIncidents);
            this.incidentsT.Controls.Add(this.label2);
            this.incidentsT.Controls.Add(this.panel3);
            this.incidentsT.Location = new System.Drawing.Point(4, 25);
            this.incidentsT.Name = "incidentsT";
            this.incidentsT.Padding = new System.Windows.Forms.Padding(3);
            this.incidentsT.Size = new System.Drawing.Size(1548, 719);
            this.incidentsT.TabIndex = 2;
            this.incidentsT.Text = "Gestion des incidents";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.label65);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Location = new System.Drawing.Point(-4, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1601, 61);
            this.panel2.TabIndex = 39;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label65.Location = new System.Drawing.Point(24, 19);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(235, 28);
            this.label65.TabIndex = 19;
            this.label65.Text = "Gestion des incidents";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Location = new System.Drawing.Point(1084, 10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(440, 41);
            this.button2.TabIndex = 3;
            this.button2.Text = "Faire une nouvelle demande d\'incident";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(277, 25);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(462, 18);
            this.label18.TabIndex = 10;
            this.label18.Text = "Sélectionner un incident dans le tableau de droite pour en voir le détail";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox4.Controls.Add(this.gbCacheDetails);
            this.groupBox4.Controls.Add(this.idIncidentSelected);
            this.groupBox4.Controls.Add(this.label_no_resol);
            this.groupBox4.Controls.Add(this.btnResol_incident);
            this.groupBox4.Controls.Add(this.date_interv);
            this.groupBox4.Controls.Add(this.technicien_incident);
            this.groupBox4.Controls.Add(this.duree_interv);
            this.groupBox4.Controls.Add(this.labDuree_interv);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.labSolution_incid);
            this.groupBox4.Controls.Add(this.labSolution_incident);
            this.groupBox4.Controls.Add(this.labDate_interv);
            this.groupBox4.Controls.Add(this.labTechn_incid);
            this.groupBox4.Controls.Add(this.objet_incident);
            this.groupBox4.Controls.Add(this.demandeur_incident);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.duree_intervention);
            this.groupBox4.Controls.Add(this.date_intervention);
            this.groupBox4.Controls.Add(this.equipement);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.date_demande);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(955, 143);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(565, 558);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            // 
            // gbCacheDetails
            // 
            this.gbCacheDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCacheDetails.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbCacheDetails.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.gbCacheDetails.Location = new System.Drawing.Point(0, 0);
            this.gbCacheDetails.Name = "gbCacheDetails";
            this.gbCacheDetails.Size = new System.Drawing.Size(565, 570);
            this.gbCacheDetails.TabIndex = 30;
            this.gbCacheDetails.TabStop = false;
            this.gbCacheDetails.Text = "Sélectionner un incident";
            // 
            // idIncidentSelected
            // 
            this.idIncidentSelected.AutoSize = true;
            this.idIncidentSelected.Location = new System.Drawing.Point(233, 327);
            this.idIncidentSelected.Name = "idIncidentSelected";
            this.idIncidentSelected.Size = new System.Drawing.Size(0, 17);
            this.idIncidentSelected.TabIndex = 29;
            this.idIncidentSelected.Visible = false;
            // 
            // label_no_resol
            // 
            this.label_no_resol.AutoSize = true;
            this.label_no_resol.Location = new System.Drawing.Point(113, 276);
            this.label_no_resol.Name = "label_no_resol";
            this.label_no_resol.Size = new System.Drawing.Size(347, 17);
            this.label_no_resol.TabIndex = 28;
            this.label_no_resol.Text = "Cet incident n\'est toujours pas en cours de résolution.";
            // 
            // btnResol_incident
            // 
            this.btnResol_incident.Location = new System.Drawing.Point(193, 347);
            this.btnResol_incident.Name = "btnResol_incident";
            this.btnResol_incident.Size = new System.Drawing.Size(176, 42);
            this.btnResol_incident.TabIndex = 27;
            this.btnResol_incident.Text = "Résoudre l\'incident";
            this.btnResol_incident.UseVisualStyleBackColor = true;
            this.btnResol_incident.Click += new System.EventHandler(this.btnResol_incident_Click);
            // 
            // date_interv
            // 
            this.date_interv.AutoSize = true;
            this.date_interv.Location = new System.Drawing.Point(171, 305);
            this.date_interv.Name = "date_interv";
            this.date_interv.Size = new System.Drawing.Size(0, 17);
            this.date_interv.TabIndex = 26;
            // 
            // technicien_incident
            // 
            this.technicien_incident.AutoSize = true;
            this.technicien_incident.Location = new System.Drawing.Point(168, 257);
            this.technicien_incident.Name = "technicien_incident";
            this.technicien_incident.Size = new System.Drawing.Size(0, 17);
            this.technicien_incident.TabIndex = 25;
            // 
            // duree_interv
            // 
            this.duree_interv.AutoSize = true;
            this.duree_interv.Location = new System.Drawing.Point(406, 306);
            this.duree_interv.Name = "duree_interv";
            this.duree_interv.Size = new System.Drawing.Size(0, 17);
            this.duree_interv.TabIndex = 24;
            // 
            // labDuree_interv
            // 
            this.labDuree_interv.AutoSize = true;
            this.labDuree_interv.Location = new System.Drawing.Point(403, 276);
            this.labDuree_interv.Name = "labDuree_interv";
            this.labDuree_interv.Size = new System.Drawing.Size(133, 17);
            this.labDuree_interv.TabIndex = 23;
            this.labDuree_interv.Text = "Durée intervention :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(122, 214);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(326, 17);
            this.label17.TabIndex = 22;
            this.label17.Text = "---------------------- RESOLUTION ----------------------";
            // 
            // labSolution_incid
            // 
            this.labSolution_incid.AutoSize = true;
            this.labSolution_incid.Location = new System.Drawing.Point(31, 347);
            this.labSolution_incid.Name = "labSolution_incid";
            this.labSolution_incid.Size = new System.Drawing.Size(67, 17);
            this.labSolution_incid.TabIndex = 21;
            this.labSolution_incid.Text = "Solution :";
            // 
            // labSolution_incident
            // 
            this.labSolution_incident.Location = new System.Drawing.Point(34, 377);
            this.labSolution_incident.Multiline = true;
            this.labSolution_incident.Name = "labSolution_incident";
            this.labSolution_incident.Size = new System.Drawing.Size(493, 122);
            this.labSolution_incident.TabIndex = 18;
            // 
            // labDate_interv
            // 
            this.labDate_interv.AutoSize = true;
            this.labDate_interv.Location = new System.Drawing.Point(31, 306);
            this.labDate_interv.Name = "labDate_interv";
            this.labDate_interv.Size = new System.Drawing.Size(124, 17);
            this.labDate_interv.TabIndex = 16;
            this.labDate_interv.Text = "Date intervention :";
            // 
            // labTechn_incid
            // 
            this.labTechn_incid.AutoSize = true;
            this.labTechn_incid.Location = new System.Drawing.Point(28, 257);
            this.labTechn_incid.Name = "labTechn_incid";
            this.labTechn_incid.Size = new System.Drawing.Size(85, 17);
            this.labTechn_incid.TabIndex = 14;
            this.labTechn_incid.Text = "Technicien :";
            // 
            // objet_incident
            // 
            this.objet_incident.AutoSize = true;
            this.objet_incident.Location = new System.Drawing.Point(168, 78);
            this.objet_incident.Name = "objet_incident";
            this.objet_incident.Size = new System.Drawing.Size(0, 17);
            this.objet_incident.TabIndex = 13;
            // 
            // demandeur_incident
            // 
            this.demandeur_incident.AutoSize = true;
            this.demandeur_incident.Location = new System.Drawing.Point(138, 116);
            this.demandeur_incident.Name = "demandeur_incident";
            this.demandeur_incident.Size = new System.Drawing.Size(0, 17);
            this.demandeur_incident.TabIndex = 12;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(28, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(90, 17);
            this.label19.TabIndex = 11;
            this.label19.Text = "Demandeur :";
            // 
            // duree_intervention
            // 
            this.duree_intervention.AutoSize = true;
            this.duree_intervention.Location = new System.Drawing.Point(423, 163);
            this.duree_intervention.Name = "duree_intervention";
            this.duree_intervention.Size = new System.Drawing.Size(0, 17);
            this.duree_intervention.TabIndex = 10;
            // 
            // date_intervention
            // 
            this.date_intervention.AutoSize = true;
            this.date_intervention.Location = new System.Drawing.Point(168, 163);
            this.date_intervention.Name = "date_intervention";
            this.date_intervention.Size = new System.Drawing.Size(0, 17);
            this.date_intervention.TabIndex = 8;
            // 
            // equipement
            // 
            this.equipement.AutoSize = true;
            this.equipement.Location = new System.Drawing.Point(138, 163);
            this.equipement.Name = "equipement";
            this.equipement.Size = new System.Drawing.Size(0, 17);
            this.equipement.TabIndex = 6;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(28, 163);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 17);
            this.label15.TabIndex = 5;
            this.label15.Text = "Equipement :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(28, 78);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 17);
            this.label16.TabIndex = 3;
            this.label16.Text = "Objet :";
            // 
            // date_demande
            // 
            this.date_demande.AutoSize = true;
            this.date_demande.Location = new System.Drawing.Point(168, 39);
            this.date_demande.Name = "date_demande";
            this.date_demande.Size = new System.Drawing.Size(0, 17);
            this.date_demande.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Date demande :";
            // 
            // dgvIncidents
            // 
            this.dgvIncidents.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgvIncidents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvIncidents.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvIncidents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIncidents.Location = new System.Drawing.Point(29, 143);
            this.dgvIncidents.Name = "dgvIncidents";
            this.dgvIncidents.RowTemplate.Height = 24;
            this.dgvIncidents.Size = new System.Drawing.Size(915, 570);
            this.dgvIncidents.TabIndex = 5;
            this.dgvIncidents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 306);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 41);
            this.label2.TabIndex = 2;
            this.label2.Text = "Liste des incidents";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.btnFiltreNiveau);
            this.panel3.Controls.Add(this.cbFiltreStatut);
            this.panel3.Controls.Add(this.cbfiltreNiveau);
            this.panel3.Controls.Add(this.btnFiltreStatut);
            this.panel3.Location = new System.Drawing.Point(29, 72);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(915, 61);
            this.panel3.TabIndex = 40;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(24, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 28);
            this.label7.TabIndex = 19;
            this.label7.Text = "Filtrer :";
            // 
            // btnFiltreNiveau
            // 
            this.btnFiltreNiveau.AutoSize = true;
            this.btnFiltreNiveau.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnFiltreNiveau.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.btnFiltreNiveau.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnFiltreNiveau.Location = new System.Drawing.Point(785, 22);
            this.btnFiltreNiveau.Name = "btnFiltreNiveau";
            this.btnFiltreNiveau.Size = new System.Drawing.Size(46, 32);
            this.btnFiltreNiveau.TabIndex = 12;
            this.btnFiltreNiveau.Text = "OK";
            this.btnFiltreNiveau.UseVisualStyleBackColor = false;
            this.btnFiltreNiveau.Click += new System.EventHandler(this.btnFiltreNiveau_Click);
            // 
            // cbFiltreStatut
            // 
            this.cbFiltreStatut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltreStatut.FormattingEnabled = true;
            this.cbFiltreStatut.Location = new System.Drawing.Point(163, 24);
            this.cbFiltreStatut.Name = "cbFiltreStatut";
            this.cbFiltreStatut.Size = new System.Drawing.Size(245, 24);
            this.cbFiltreStatut.TabIndex = 6;
            // 
            // cbfiltreNiveau
            // 
            this.cbfiltreNiveau.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbfiltreNiveau.FormattingEnabled = true;
            this.cbfiltreNiveau.Location = new System.Drawing.Point(534, 24);
            this.cbfiltreNiveau.Name = "cbfiltreNiveau";
            this.cbfiltreNiveau.Size = new System.Drawing.Size(245, 24);
            this.cbfiltreNiveau.TabIndex = 11;
            // 
            // btnFiltreStatut
            // 
            this.btnFiltreStatut.AutoSize = true;
            this.btnFiltreStatut.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnFiltreStatut.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.btnFiltreStatut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnFiltreStatut.Location = new System.Drawing.Point(414, 22);
            this.btnFiltreStatut.Name = "btnFiltreStatut";
            this.btnFiltreStatut.Size = new System.Drawing.Size(46, 32);
            this.btnFiltreStatut.TabIndex = 9;
            this.btnFiltreStatut.Text = "OK";
            this.btnFiltreStatut.UseVisualStyleBackColor = false;
            this.btnFiltreStatut.Click += new System.EventHandler(this.button5_Click);
            // 
            // celluleAssistanceT
            // 
            this.celluleAssistanceT.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.celluleAssistanceT.Controls.Add(this.btnValiderStatsGraph);
            this.celluleAssistanceT.Controls.Add(this.cbYearChart);
            this.celluleAssistanceT.Controls.Add(this.label11);
            this.celluleAssistanceT.Controls.Add(this.cbMonthChart);
            this.celluleAssistanceT.Controls.Add(this.chartNbIncidentMonth);
            this.celluleAssistanceT.Controls.Add(this.nbIncidentsResolusMonth);
            this.celluleAssistanceT.Controls.Add(this.label56);
            this.celluleAssistanceT.Controls.Add(this.nbEquipAffectesMonth);
            this.celluleAssistanceT.Controls.Add(this.label41);
            this.celluleAssistanceT.Controls.Add(this.panel4);
            this.celluleAssistanceT.Controls.Add(this.dureeMoyResol);
            this.celluleAssistanceT.Controls.Add(this.nbIncidentsResolus);
            this.celluleAssistanceT.Controls.Add(this.nbIncidents);
            this.celluleAssistanceT.Controls.Add(this.label60);
            this.celluleAssistanceT.Controls.Add(this.label59);
            this.celluleAssistanceT.Controls.Add(this.label58);
            this.celluleAssistanceT.Controls.Add(this.nbEquipementsAffectes);
            this.celluleAssistanceT.Controls.Add(this.nbEquipements);
            this.celluleAssistanceT.Controls.Add(this.label55);
            this.celluleAssistanceT.Controls.Add(this.label54);
            this.celluleAssistanceT.Controls.Add(this.nbVisiteurs);
            this.celluleAssistanceT.Controls.Add(this.label40);
            this.celluleAssistanceT.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.celluleAssistanceT.Location = new System.Drawing.Point(4, 25);
            this.celluleAssistanceT.Name = "celluleAssistanceT";
            this.celluleAssistanceT.Size = new System.Drawing.Size(1548, 719);
            this.celluleAssistanceT.TabIndex = 3;
            this.celluleAssistanceT.Text = "Cellule d\'Assistance";
            // 
            // btnValiderStatsGraph
            // 
            this.btnValiderStatsGraph.Location = new System.Drawing.Point(1333, 140);
            this.btnValiderStatsGraph.Name = "btnValiderStatsGraph";
            this.btnValiderStatsGraph.Size = new System.Drawing.Size(75, 23);
            this.btnValiderStatsGraph.TabIndex = 49;
            this.btnValiderStatsGraph.Text = "Valider";
            this.btnValiderStatsGraph.UseVisualStyleBackColor = true;
            this.btnValiderStatsGraph.Click += new System.EventHandler(this.btnValiderStatsGraph_Click);
            // 
            // cbYearChart
            // 
            this.cbYearChart.FormattingEnabled = true;
            this.cbYearChart.Location = new System.Drawing.Point(1172, 140);
            this.cbYearChart.Name = "cbYearChart";
            this.cbYearChart.Size = new System.Drawing.Size(155, 24);
            this.cbYearChart.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1079, 105);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(284, 17);
            this.label11.TabIndex = 47;
            this.label11.Text = "Nombre de tickets par jour pour le mois de :";
            // 
            // cbMonthChart
            // 
            this.cbMonthChart.FormattingEnabled = true;
            this.cbMonthChart.Location = new System.Drawing.Point(982, 140);
            this.cbMonthChart.Name = "cbMonthChart";
            this.cbMonthChart.Size = new System.Drawing.Size(184, 24);
            this.cbMonthChart.TabIndex = 46;
            // 
            // chartNbIncidentMonth
            // 
            chartArea1.Name = "ChartArea1";
            this.chartNbIncidentMonth.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartNbIncidentMonth.Legends.Add(legend1);
            this.chartNbIncidentMonth.Location = new System.Drawing.Point(954, 182);
            this.chartNbIncidentMonth.Name = "chartNbIncidentMonth";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartNbIncidentMonth.Series.Add(series1);
            this.chartNbIncidentMonth.Size = new System.Drawing.Size(522, 394);
            this.chartNbIncidentMonth.TabIndex = 45;
            this.chartNbIncidentMonth.Text = "chart1";
            // 
            // nbIncidentsResolusMonth
            // 
            this.nbIncidentsResolusMonth.AutoSize = true;
            this.nbIncidentsResolusMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbIncidentsResolusMonth.Location = new System.Drawing.Point(714, 285);
            this.nbIncidentsResolusMonth.Name = "nbIncidentsResolusMonth";
            this.nbIncidentsResolusMonth.Size = new System.Drawing.Size(16, 18);
            this.nbIncidentsResolusMonth.TabIndex = 44;
            this.nbIncidentsResolusMonth.Text = "7";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(490, 276);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(184, 36);
            this.label56.TabIndex = 43;
            this.label56.Text = "Nombre d\'incident résolus \r\nce mois :";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nbEquipAffectesMonth
            // 
            this.nbEquipAffectesMonth.AutoSize = true;
            this.nbEquipAffectesMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbEquipAffectesMonth.Location = new System.Drawing.Point(316, 285);
            this.nbEquipAffectesMonth.Name = "nbEquipAffectesMonth";
            this.nbEquipAffectesMonth.Size = new System.Drawing.Size(16, 18);
            this.nbEquipAffectesMonth.TabIndex = 42;
            this.nbEquipAffectesMonth.Text = "7";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(65, 276);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(221, 36);
            this.label41.TabIndex = 41;
            this.label41.Text = "Nombre d\'équipements affectés \r\nce mois :";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel4.Controls.Add(this.label3);
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1601, 61);
            this.panel4.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(24, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 28);
            this.label3.TabIndex = 19;
            this.label3.Text = "Statistiques";
            // 
            // dureeMoyResol
            // 
            this.dureeMoyResol.AutoSize = true;
            this.dureeMoyResol.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dureeMoyResol.Location = new System.Drawing.Point(724, 231);
            this.dureeMoyResol.Name = "dureeMoyResol";
            this.dureeMoyResol.Size = new System.Drawing.Size(53, 18);
            this.dureeMoyResol.TabIndex = 15;
            this.dureeMoyResol.Text = "2 jours";
            // 
            // nbIncidentsResolus
            // 
            this.nbIncidentsResolus.AutoSize = true;
            this.nbIncidentsResolus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbIncidentsResolus.Location = new System.Drawing.Point(706, 182);
            this.nbIncidentsResolus.Name = "nbIncidentsResolus";
            this.nbIncidentsResolus.Size = new System.Drawing.Size(24, 18);
            this.nbIncidentsResolus.TabIndex = 14;
            this.nbIncidentsResolus.Text = "18";
            // 
            // nbIncidents
            // 
            this.nbIncidents.AutoSize = true;
            this.nbIncidents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbIncidents.Location = new System.Drawing.Point(662, 131);
            this.nbIncidents.Name = "nbIncidents";
            this.nbIncidents.Size = new System.Drawing.Size(24, 18);
            this.nbIncidents.TabIndex = 13;
            this.nbIncidents.Text = "21";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(490, 182);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(196, 18);
            this.label60.TabIndex = 12;
            this.label60.Text = "Nombre d\'incidents résolus :";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(490, 131);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(143, 18);
            this.label59.TabIndex = 11;
            this.label59.Text = "Nombre d\'incidents :";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(490, 231);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(210, 18);
            this.label58.TabIndex = 10;
            this.label58.Text = "Durée moyenne de résolution :";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nbEquipementsAffectes
            // 
            this.nbEquipementsAffectes.AutoSize = true;
            this.nbEquipementsAffectes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbEquipementsAffectes.Location = new System.Drawing.Point(316, 231);
            this.nbEquipementsAffectes.Name = "nbEquipementsAffectes";
            this.nbEquipementsAffectes.Size = new System.Drawing.Size(16, 18);
            this.nbEquipementsAffectes.TabIndex = 9;
            this.nbEquipementsAffectes.Text = "7";
            // 
            // nbEquipements
            // 
            this.nbEquipements.AutoSize = true;
            this.nbEquipements.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbEquipements.Location = new System.Drawing.Point(267, 182);
            this.nbEquipements.Name = "nbEquipements";
            this.nbEquipements.Size = new System.Drawing.Size(24, 18);
            this.nbEquipements.TabIndex = 8;
            this.nbEquipements.Text = "10";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(66, 231);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(225, 18);
            this.label55.TabIndex = 7;
            this.label55.Text = "Nombre d\'équipements affectés :";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(65, 182);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(169, 18);
            this.label54.TabIndex = 6;
            this.label54.Text = "Nombre d\'équipements :";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nbVisiteurs
            // 
            this.nbVisiteurs.AutoSize = true;
            this.nbVisiteurs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbVisiteurs.Location = new System.Drawing.Point(249, 131);
            this.nbVisiteurs.Name = "nbVisiteurs";
            this.nbVisiteurs.Size = new System.Drawing.Size(54, 18);
            this.nbVisiteurs.TabIndex = 5;
            this.nbVisiteurs.Text = "label41";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(65, 131);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(150, 18);
            this.label40.TabIndex = 4;
            this.label40.Text = "Nombre de Visiteurs :";
            // 
            // configurationA
            // 
            this.configurationA.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.configurationA.Controls.Add(this.panel5);
            this.configurationA.Controls.Add(this.groupBox12);
            this.configurationA.Controls.Add(this.groupBox11);
            this.configurationA.Controls.Add(this.groupBox9);
            this.configurationA.Controls.Add(this.groupBox8);
            this.configurationA.Controls.Add(this.groupBox10);
            this.configurationA.Controls.Add(this.groupBox6);
            this.configurationA.Controls.Add(this.groupBox7);
            this.configurationA.Controls.Add(this.groupBox5);
            this.configurationA.Controls.Add(this.groupBox3);
            this.configurationA.Controls.Add(this.groupBox2);
            this.configurationA.Controls.Add(this.groupBox1);
            this.configurationA.Location = new System.Drawing.Point(4, 25);
            this.configurationA.Name = "configurationA";
            this.configurationA.Size = new System.Drawing.Size(1548, 719);
            this.configurationA.TabIndex = 4;
            this.configurationA.Text = "Configuration";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel5.Controls.Add(this.label5);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1601, 61);
            this.panel5.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(24, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 28);
            this.label5.TabIndex = 19;
            this.label5.Text = "Administration";
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox12.Controls.Add(this.btnSaveUniteComposants);
            this.groupBox12.Controls.Add(this.tbUniteComposA);
            this.groupBox12.Controls.Add(this.label39);
            this.groupBox12.ForeColor = System.Drawing.Color.Black;
            this.groupBox12.Location = new System.Drawing.Point(1185, 230);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(335, 150);
            this.groupBox12.TabIndex = 22;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Gestion des unités des composants";
            // 
            // btnSaveUniteComposants
            // 
            this.btnSaveUniteComposants.Location = new System.Drawing.Point(125, 89);
            this.btnSaveUniteComposants.Name = "btnSaveUniteComposants";
            this.btnSaveUniteComposants.Size = new System.Drawing.Size(92, 35);
            this.btnSaveUniteComposants.TabIndex = 17;
            this.btnSaveUniteComposants.Text = "Enregistrer";
            this.btnSaveUniteComposants.UseVisualStyleBackColor = true;
            this.btnSaveUniteComposants.Click += new System.EventHandler(this.btnSaveUniteComposants_Click);
            // 
            // tbUniteComposA
            // 
            this.tbUniteComposA.Location = new System.Drawing.Point(125, 46);
            this.tbUniteComposA.Name = "tbUniteComposA";
            this.tbUniteComposA.Size = new System.Drawing.Size(188, 22);
            this.tbUniteComposA.TabIndex = 19;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(30, 48);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(49, 17);
            this.label39.TabIndex = 18;
            this.label39.Text = "Unité :";
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.groupBox11.Controls.Add(this.btnSaveStatutIncidents);
            this.groupBox11.Controls.Add(this.tbStatutIncidA);
            this.groupBox11.Controls.Add(this.label38);
            this.groupBox11.Location = new System.Drawing.Point(1185, 398);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(335, 150);
            this.groupBox11.TabIndex = 21;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Gestion des statuts des incidents";
            // 
            // btnSaveStatutIncidents
            // 
            this.btnSaveStatutIncidents.Location = new System.Drawing.Point(125, 89);
            this.btnSaveStatutIncidents.Name = "btnSaveStatutIncidents";
            this.btnSaveStatutIncidents.Size = new System.Drawing.Size(92, 35);
            this.btnSaveStatutIncidents.TabIndex = 17;
            this.btnSaveStatutIncidents.Text = "Enregistrer";
            this.btnSaveStatutIncidents.UseVisualStyleBackColor = true;
            this.btnSaveStatutIncidents.Click += new System.EventHandler(this.btnSaveStatutIncidents_Click);
            // 
            // tbStatutIncidA
            // 
            this.tbStatutIncidA.Location = new System.Drawing.Point(125, 46);
            this.tbStatutIncidA.Name = "tbStatutIncidA";
            this.tbStatutIncidA.Size = new System.Drawing.Size(188, 22);
            this.tbStatutIncidA.TabIndex = 19;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(30, 48);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(57, 17);
            this.label38.TabIndex = 18;
            this.label38.Text = "Statut : ";
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.groupBox9.Controls.Add(this.btnSaveMarques);
            this.groupBox9.Controls.Add(this.tbMarque);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Location = new System.Drawing.Point(799, 509);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(329, 150);
            this.groupBox9.TabIndex = 20;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Gestion des marques";
            // 
            // btnSaveMarques
            // 
            this.btnSaveMarques.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSaveMarques.Location = new System.Drawing.Point(125, 89);
            this.btnSaveMarques.Name = "btnSaveMarques";
            this.btnSaveMarques.Size = new System.Drawing.Size(92, 35);
            this.btnSaveMarques.TabIndex = 17;
            this.btnSaveMarques.Text = "Enregistrer";
            this.btnSaveMarques.UseVisualStyleBackColor = false;
            this.btnSaveMarques.Click += new System.EventHandler(this.btnSaveMarques_Click);
            // 
            // tbMarque
            // 
            this.tbMarque.Location = new System.Drawing.Point(125, 46);
            this.tbMarque.Name = "tbMarque";
            this.tbMarque.Size = new System.Drawing.Size(188, 22);
            this.tbMarque.TabIndex = 19;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(30, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(45, 17);
            this.label35.TabIndex = 18;
            this.label35.Text = "Nom :";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBox8.Controls.Add(this.btnSaveProfils);
            this.groupBox8.Controls.Add(this.tbProfilA);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Location = new System.Drawing.Point(29, 514);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(335, 150);
            this.groupBox8.TabIndex = 20;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Gestion des profils utilisateurs";
            // 
            // btnSaveProfils
            // 
            this.btnSaveProfils.Location = new System.Drawing.Point(125, 89);
            this.btnSaveProfils.Name = "btnSaveProfils";
            this.btnSaveProfils.Size = new System.Drawing.Size(92, 35);
            this.btnSaveProfils.TabIndex = 17;
            this.btnSaveProfils.Text = "Enregistrer";
            this.btnSaveProfils.UseVisualStyleBackColor = true;
            this.btnSaveProfils.Click += new System.EventHandler(this.btnSaveProfils_Click);
            // 
            // tbProfilA
            // 
            this.tbProfilA.Location = new System.Drawing.Point(125, 46);
            this.tbProfilA.Name = "tbProfilA";
            this.tbProfilA.Size = new System.Drawing.Size(188, 22);
            this.tbProfilA.TabIndex = 19;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(30, 48);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(52, 17);
            this.label37.TabIndex = 18;
            this.label37.Text = "Profil : ";
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Silver;
            this.groupBox10.Controls.Add(this.btnSaveNiveauIncidents);
            this.groupBox10.Controls.Add(this.tbNivIncidA);
            this.groupBox10.Controls.Add(this.label36);
            this.groupBox10.Location = new System.Drawing.Point(1185, 560);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(335, 150);
            this.groupBox10.TabIndex = 21;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Gestion des niveaux de difficulté d\'incidents";
            // 
            // btnSaveNiveauIncidents
            // 
            this.btnSaveNiveauIncidents.Location = new System.Drawing.Point(125, 89);
            this.btnSaveNiveauIncidents.Name = "btnSaveNiveauIncidents";
            this.btnSaveNiveauIncidents.Size = new System.Drawing.Size(92, 35);
            this.btnSaveNiveauIncidents.TabIndex = 17;
            this.btnSaveNiveauIncidents.Text = "Enregistrer";
            this.btnSaveNiveauIncidents.UseVisualStyleBackColor = true;
            this.btnSaveNiveauIncidents.Click += new System.EventHandler(this.btnSaveNiveauIncidents_Click);
            // 
            // tbNivIncidA
            // 
            this.tbNivIncidA.Location = new System.Drawing.Point(125, 46);
            this.tbNivIncidA.Name = "tbNivIncidA";
            this.tbNivIncidA.Size = new System.Drawing.Size(188, 22);
            this.tbNivIncidA.TabIndex = 19;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(30, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(60, 17);
            this.label36.TabIndex = 18;
            this.label36.Text = "Niveau :";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox6.Controls.Add(this.btnSaveEtatEquipements);
            this.groupBox6.Controls.Add(this.tbEtatEquipA);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Location = new System.Drawing.Point(799, 307);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(329, 150);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Gestion des états des équipements";
            // 
            // btnSaveEtatEquipements
            // 
            this.btnSaveEtatEquipements.Location = new System.Drawing.Point(125, 89);
            this.btnSaveEtatEquipements.Name = "btnSaveEtatEquipements";
            this.btnSaveEtatEquipements.Size = new System.Drawing.Size(92, 35);
            this.btnSaveEtatEquipements.TabIndex = 17;
            this.btnSaveEtatEquipements.Text = "Enregistrer";
            this.btnSaveEtatEquipements.UseVisualStyleBackColor = true;
            this.btnSaveEtatEquipements.Click += new System.EventHandler(this.btnSaveEtatEquipements_Click);
            // 
            // tbEtatEquipA
            // 
            this.tbEtatEquipA.Location = new System.Drawing.Point(125, 46);
            this.tbEtatEquipA.Name = "tbEtatEquipA";
            this.tbEtatEquipA.Size = new System.Drawing.Size(188, 22);
            this.tbEtatEquipA.TabIndex = 19;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(30, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 17);
            this.label34.TabIndex = 18;
            this.label34.Text = "Etat :";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Cyan;
            this.groupBox7.Controls.Add(this.btnSaveTypeComposants);
            this.groupBox7.Controls.Add(this.tbTypeComposA);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Location = new System.Drawing.Point(1185, 67);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(335, 150);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Gestion des types de composants";
            // 
            // btnSaveTypeComposants
            // 
            this.btnSaveTypeComposants.Location = new System.Drawing.Point(125, 89);
            this.btnSaveTypeComposants.Name = "btnSaveTypeComposants";
            this.btnSaveTypeComposants.Size = new System.Drawing.Size(92, 35);
            this.btnSaveTypeComposants.TabIndex = 17;
            this.btnSaveTypeComposants.Text = "Enregistrer";
            this.btnSaveTypeComposants.UseVisualStyleBackColor = true;
            this.btnSaveTypeComposants.Click += new System.EventHandler(this.btnSaveTypeComposants_Click);
            // 
            // tbTypeComposA
            // 
            this.tbTypeComposA.Location = new System.Drawing.Point(125, 46);
            this.tbTypeComposA.Name = "tbTypeComposA";
            this.tbTypeComposA.Size = new System.Drawing.Size(188, 22);
            this.tbTypeComposA.TabIndex = 19;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(30, 48);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(48, 17);
            this.label28.TabIndex = 18;
            this.label28.Text = "Type :";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.groupBox5.Controls.Add(this.btnSaveLogicielEquipements);
            this.groupBox5.Controls.Add(this.textBox11);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.textBox10);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.textBox9);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(415, 445);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(329, 237);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Gestion des logiciels d\'équipement";
            // 
            // btnSaveLogicielEquipements
            // 
            this.btnSaveLogicielEquipements.Location = new System.Drawing.Point(111, 176);
            this.btnSaveLogicielEquipements.Name = "btnSaveLogicielEquipements";
            this.btnSaveLogicielEquipements.Size = new System.Drawing.Size(92, 35);
            this.btnSaveLogicielEquipements.TabIndex = 17;
            this.btnSaveLogicielEquipements.Text = "Enregistrer";
            this.btnSaveLogicielEquipements.UseVisualStyleBackColor = true;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(111, 130);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(188, 22);
            this.textBox11.TabIndex = 22;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 132);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(64, 17);
            this.label31.TabIndex = 21;
            this.label31.Text = "Version :";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(111, 90);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(188, 22);
            this.textBox10.TabIndex = 20;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(16, 92);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(61, 17);
            this.label30.TabIndex = 19;
            this.label30.Text = "Editeur :";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(111, 47);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(188, 22);
            this.textBox9.TabIndex = 18;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(45, 17);
            this.label29.TabIndex = 17;
            this.label29.Text = "Nom :";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.groupBox3.Controls.Add(this.btnSaveTypeEquipements);
            this.groupBox3.Controls.Add(this.tbTypeEquipements);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Location = new System.Drawing.Point(799, 113);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(329, 150);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gestion des types d\'équipement";
            // 
            // btnSaveTypeEquipements
            // 
            this.btnSaveTypeEquipements.Location = new System.Drawing.Point(125, 90);
            this.btnSaveTypeEquipements.Name = "btnSaveTypeEquipements";
            this.btnSaveTypeEquipements.Size = new System.Drawing.Size(92, 35);
            this.btnSaveTypeEquipements.TabIndex = 15;
            this.btnSaveTypeEquipements.Text = "Enregistrer";
            this.btnSaveTypeEquipements.UseVisualStyleBackColor = true;
            this.btnSaveTypeEquipements.Click += new System.EventHandler(this.btnSaveTypeEquipements_Click);
            // 
            // tbTypeEquipements
            // 
            this.tbTypeEquipements.Location = new System.Drawing.Point(125, 47);
            this.tbTypeEquipements.Name = "tbTypeEquipements";
            this.tbTypeEquipements.Size = new System.Drawing.Size(188, 22);
            this.tbTypeEquipements.TabIndex = 16;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(30, 49);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 17);
            this.label27.TabIndex = 15;
            this.label27.Text = "Type :";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.groupBox2.Controls.Add(this.tbMailFournissA);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.tbTelFournissA);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.btnSaveFournisseurs);
            this.groupBox2.Controls.Add(this.tbAdrFournissA);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.tbNomFournissA);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Location = new System.Drawing.Point(415, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(329, 322);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Gestion des Fournisseurs";
            // 
            // tbMailFournissA
            // 
            this.tbMailFournissA.Location = new System.Drawing.Point(123, 215);
            this.tbMailFournissA.Name = "tbMailFournissA";
            this.tbMailFournissA.Size = new System.Drawing.Size(184, 22);
            this.tbMailFournissA.TabIndex = 17;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(24, 218);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 17);
            this.label33.TabIndex = 16;
            this.label33.Text = "Mail :";
            // 
            // tbTelFournissA
            // 
            this.tbTelFournissA.Location = new System.Drawing.Point(123, 171);
            this.tbTelFournissA.Name = "tbTelFournissA";
            this.tbTelFournissA.Size = new System.Drawing.Size(184, 22);
            this.tbTelFournissA.TabIndex = 15;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(24, 174);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 17);
            this.label32.TabIndex = 14;
            this.label32.Text = "Tel :";
            // 
            // btnSaveFournisseurs
            // 
            this.btnSaveFournisseurs.Location = new System.Drawing.Point(119, 265);
            this.btnSaveFournisseurs.Name = "btnSaveFournisseurs";
            this.btnSaveFournisseurs.Size = new System.Drawing.Size(92, 35);
            this.btnSaveFournisseurs.TabIndex = 14;
            this.btnSaveFournisseurs.Text = "Enregistrer";
            this.btnSaveFournisseurs.UseVisualStyleBackColor = true;
            this.btnSaveFournisseurs.Click += new System.EventHandler(this.btnSaveFournisseurs_Click);
            // 
            // tbAdrFournissA
            // 
            this.tbAdrFournissA.Location = new System.Drawing.Point(119, 87);
            this.tbAdrFournissA.Multiline = true;
            this.tbAdrFournissA.Name = "tbAdrFournissA";
            this.tbAdrFournissA.Size = new System.Drawing.Size(188, 63);
            this.tbAdrFournissA.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(24, 90);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(68, 17);
            this.label26.TabIndex = 2;
            this.label26.Text = "Adresse :";
            // 
            // tbNomFournissA
            // 
            this.tbNomFournissA.Location = new System.Drawing.Point(119, 47);
            this.tbNomFournissA.Name = "tbNomFournissA";
            this.tbNomFournissA.Size = new System.Drawing.Size(188, 22);
            this.tbNomFournissA.TabIndex = 1;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(24, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 17);
            this.label25.TabIndex = 0;
            this.label25.Text = "Nom :";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.groupBox1.Controls.Add(this.btnSaveUtilisateur);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.tbPasswordA);
            this.groupBox1.Controls.Add(this.tbMailA);
            this.groupBox1.Controls.Add(this.tbPrenomA);
            this.groupBox1.Controls.Add(this.tbNomA);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.cbProfils);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Location = new System.Drawing.Point(29, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 360);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gestion des Utilisateurs";
            // 
            // btnSaveUtilisateur
            // 
            this.btnSaveUtilisateur.Location = new System.Drawing.Point(105, 307);
            this.btnSaveUtilisateur.Name = "btnSaveUtilisateur";
            this.btnSaveUtilisateur.Size = new System.Drawing.Size(92, 35);
            this.btnSaveUtilisateur.TabIndex = 13;
            this.btnSaveUtilisateur.Text = "Enregistrer";
            this.btnSaveUtilisateur.UseVisualStyleBackColor = true;
            this.btnSaveUtilisateur.Click += new System.EventHandler(this.btnSaveUtilisateur_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(127, 262);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(184, 23);
            this.button6.TabIndex = 12;
            this.button6.Text = "Parcourir ...";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(18, 265);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 17);
            this.label24.TabIndex = 11;
            this.label24.Text = "Avatar :";
            this.label24.Visible = false;
            // 
            // tbPasswordA
            // 
            this.tbPasswordA.Location = new System.Drawing.Point(127, 173);
            this.tbPasswordA.Name = "tbPasswordA";
            this.tbPasswordA.PasswordChar = '*';
            this.tbPasswordA.Size = new System.Drawing.Size(184, 22);
            this.tbPasswordA.TabIndex = 9;
            this.tbPasswordA.UseSystemPasswordChar = true;
            // 
            // tbMailA
            // 
            this.tbMailA.Location = new System.Drawing.Point(127, 130);
            this.tbMailA.Name = "tbMailA";
            this.tbMailA.Size = new System.Drawing.Size(184, 22);
            this.tbMailA.TabIndex = 8;
            // 
            // tbPrenomA
            // 
            this.tbPrenomA.Location = new System.Drawing.Point(127, 90);
            this.tbPrenomA.Name = "tbPrenomA";
            this.tbPrenomA.Size = new System.Drawing.Size(184, 22);
            this.tbPrenomA.TabIndex = 7;
            // 
            // tbNomA
            // 
            this.tbNomA.Location = new System.Drawing.Point(127, 47);
            this.tbNomA.Name = "tbNomA";
            this.tbNomA.Size = new System.Drawing.Size(184, 22);
            this.tbNomA.TabIndex = 6;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(18, 220);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 17);
            this.label23.TabIndex = 5;
            this.label23.Text = "Profil : ";
            // 
            // cbProfils
            // 
            this.cbProfils.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProfils.FormattingEnabled = true;
            this.cbProfils.Location = new System.Drawing.Point(127, 220);
            this.cbProfils.Name = "cbProfils";
            this.cbProfils.Size = new System.Drawing.Size(184, 24);
            this.cbProfils.TabIndex = 4;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(18, 176);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(101, 17);
            this.label22.TabIndex = 3;
            this.label22.Text = "Mot de passe :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 133);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 17);
            this.label21.TabIndex = 2;
            this.label21.Text = "Mail :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(18, 93);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 17);
            this.label20.TabIndex = 1;
            this.label20.Text = "Prénom :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Nom :";
            // 
            // celluleAssistanceV
            // 
            this.celluleAssistanceV.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.celluleAssistanceV.Controls.Add(this.panel6);
            this.celluleAssistanceV.Controls.Add(this.groupBox14);
            this.celluleAssistanceV.Controls.Add(this.groupBox13);
            this.celluleAssistanceV.Location = new System.Drawing.Point(4, 25);
            this.celluleAssistanceV.Name = "celluleAssistanceV";
            this.celluleAssistanceV.Size = new System.Drawing.Size(1548, 719);
            this.celluleAssistanceV.TabIndex = 6;
            this.celluleAssistanceV.Text = "Cellule d\'Assistance";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel6.Controls.Add(this.label66);
            this.panel6.Location = new System.Drawing.Point(-4, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1608, 61);
            this.panel6.TabIndex = 42;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label66.Location = new System.Drawing.Point(24, 19);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(267, 28);
            this.label66.TabIndex = 19;
            this.label66.Text = "Tableau de bord visiteur";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.lbEquipements);
            this.groupBox14.Controls.Add(this.button7);
            this.groupBox14.Controls.Add(this.label51);
            this.groupBox14.Controls.Add(this.labNbEquipV);
            this.groupBox14.Font = new System.Drawing.Font("Gill Sans MT Condensed", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.groupBox14.Location = new System.Drawing.Point(868, 143);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(494, 371);
            this.groupBox14.TabIndex = 10;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Mes équipements";
            // 
            // lbEquipements
            // 
            this.lbEquipements.FormattingEnabled = true;
            this.lbEquipements.ItemHeight = 37;
            this.lbEquipements.Location = new System.Drawing.Point(21, 44);
            this.lbEquipements.Name = "lbEquipements";
            this.lbEquipements.Size = new System.Drawing.Size(449, 152);
            this.lbEquipements.TabIndex = 10;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(40, 288);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(405, 65);
            this.button7.TabIndex = 9;
            this.button7.Text = "Voir le détail de mon équipement";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(33, 218);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(88, 41);
            this.label51.TabIndex = 4;
            this.label51.Text = "Total :";
            // 
            // labNbEquipV
            // 
            this.labNbEquipV.AutoSize = true;
            this.labNbEquipV.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNbEquipV.Location = new System.Drawing.Point(186, 218);
            this.labNbEquipV.Name = "labNbEquipV";
            this.labNbEquipV.Size = new System.Drawing.Size(172, 41);
            this.labNbEquipV.TabIndex = 5;
            this.labNbEquipV.Text = "3 équipements";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.button5);
            this.groupBox13.Controls.Add(this.labNbIncidAttenteV);
            this.groupBox13.Controls.Add(this.label45);
            this.groupBox13.Controls.Add(this.labNbIncidResolV);
            this.groupBox13.Controls.Add(this.label43);
            this.groupBox13.Controls.Add(this.labNbDemandesIncidV);
            this.groupBox13.Font = new System.Drawing.Font("Gill Sans MT Condensed", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.groupBox13.Location = new System.Drawing.Point(158, 143);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(494, 371);
            this.groupBox13.TabIndex = 6;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Mes incidents";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(120, 288);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(221, 65);
            this.button5.TabIndex = 9;
            this.button5.Text = "Voir mes Incidents";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // labNbIncidAttenteV
            // 
            this.labNbIncidAttenteV.AutoSize = true;
            this.labNbIncidAttenteV.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNbIncidAttenteV.Location = new System.Drawing.Point(173, 195);
            this.labNbIncidAttenteV.Name = "labNbIncidAttenteV";
            this.labNbIncidAttenteV.Size = new System.Drawing.Size(269, 41);
            this.labNbIncidAttenteV.TabIndex = 8;
            this.labNbIncidAttenteV.Text = "- 1 incident en attente";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(20, 145);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(86, 41);
            this.label45.TabIndex = 6;
            this.label45.Text = "Dont :";
            // 
            // labNbIncidResolV
            // 
            this.labNbIncidResolV.AutoSize = true;
            this.labNbIncidResolV.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNbIncidResolV.Location = new System.Drawing.Point(173, 145);
            this.labNbIncidResolV.Name = "labNbIncidResolV";
            this.labNbIncidResolV.Size = new System.Drawing.Size(238, 41);
            this.labNbIncidResolV.TabIndex = 7;
            this.labNbIncidResolV.Text = "- 2 incidents résolus";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(20, 67);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(128, 41);
            this.label43.TabIndex = 4;
            this.label43.Text = "En cours :";
            // 
            // labNbDemandesIncidV
            // 
            this.labNbDemandesIncidV.AutoSize = true;
            this.labNbDemandesIncidV.Font = new System.Drawing.Font("Gill Sans MT Condensed", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labNbDemandesIncidV.Location = new System.Drawing.Point(173, 67);
            this.labNbDemandesIncidV.Name = "labNbDemandesIncidV";
            this.labNbDemandesIncidV.Size = new System.Drawing.Size(268, 41);
            this.labNbDemandesIncidV.TabIndex = 5;
            this.labNbDemandesIncidV.Text = "3 demandes d\'incidents";
            // 
            // incidentsV
            // 
            this.incidentsV.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.incidentsV.Controls.Add(this.panel7);
            this.incidentsV.Controls.Add(this.groupBox17);
            this.incidentsV.Controls.Add(this.groupBox16);
            this.incidentsV.Controls.Add(this.dgvSuiviIncidentsV);
            this.incidentsV.Location = new System.Drawing.Point(4, 25);
            this.incidentsV.Name = "incidentsV";
            this.incidentsV.Size = new System.Drawing.Size(1548, 719);
            this.incidentsV.TabIndex = 5;
            this.incidentsV.Text = "Mes incidents";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel7.Controls.Add(this.label42);
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1608, 61);
            this.panel7.TabIndex = 43;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label42.Location = new System.Drawing.Point(24, 19);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(244, 28);
            this.label42.TabIndex = 19;
            this.label42.Text = "Suivi de mes incidents";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.labDateResolIncidV);
            this.groupBox17.Controls.Add(this.label80);
            this.groupBox17.Controls.Add(this.labTechnicIncidV);
            this.groupBox17.Controls.Add(this.label77);
            this.groupBox17.Controls.Add(this.tbResolIncidV);
            this.groupBox17.Location = new System.Drawing.Point(817, 271);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(600, 405);
            this.groupBox17.TabIndex = 6;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Solution";
            // 
            // labDateResolIncidV
            // 
            this.labDateResolIncidV.AutoSize = true;
            this.labDateResolIncidV.Location = new System.Drawing.Point(277, 108);
            this.labDateResolIncidV.Name = "labDateResolIncidV";
            this.labDateResolIncidV.Size = new System.Drawing.Size(109, 17);
            this.labDateResolIncidV.TabIndex = 30;
            this.labDateResolIncidV.Text = "Date demande :";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(100, 108);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(132, 17);
            this.label80.TabIndex = 29;
            this.label80.Text = "Date de résolution :";
            // 
            // labTechnicIncidV
            // 
            this.labTechnicIncidV.AutoSize = true;
            this.labTechnicIncidV.Location = new System.Drawing.Point(277, 67);
            this.labTechnicIncidV.Name = "labTechnicIncidV";
            this.labTechnicIncidV.Size = new System.Drawing.Size(109, 17);
            this.labTechnicIncidV.TabIndex = 28;
            this.labTechnicIncidV.Text = "Date demande :";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(100, 67);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(85, 17);
            this.label77.TabIndex = 27;
            this.label77.Text = "Technicien :";
            // 
            // tbResolIncidV
            // 
            this.tbResolIncidV.Enabled = false;
            this.tbResolIncidV.Location = new System.Drawing.Point(103, 150);
            this.tbResolIncidV.Multiline = true;
            this.tbResolIncidV.Name = "tbResolIncidV";
            this.tbResolIncidV.Size = new System.Drawing.Size(423, 230);
            this.tbResolIncidV.TabIndex = 0;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.labStatutIncidV);
            this.groupBox16.Controls.Add(this.label74);
            this.groupBox16.Controls.Add(this.labIdIncidV);
            this.groupBox16.Controls.Add(this.label72);
            this.groupBox16.Controls.Add(this.labEquipIncidV);
            this.groupBox16.Controls.Add(this.labDemandeurIncidV);
            this.groupBox16.Controls.Add(this.labObjIncidV);
            this.groupBox16.Controls.Add(this.labDateDemandeIncidV);
            this.groupBox16.Controls.Add(this.label73);
            this.groupBox16.Controls.Add(this.label75);
            this.groupBox16.Controls.Add(this.label76);
            this.groupBox16.Controls.Add(this.label78);
            this.groupBox16.Location = new System.Drawing.Point(104, 271);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(600, 405);
            this.groupBox16.TabIndex = 5;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Détails";
            // 
            // labStatutIncidV
            // 
            this.labStatutIncidV.AutoSize = true;
            this.labStatutIncidV.Location = new System.Drawing.Point(311, 108);
            this.labStatutIncidV.Name = "labStatutIncidV";
            this.labStatutIncidV.Size = new System.Drawing.Size(109, 17);
            this.labStatutIncidV.TabIndex = 28;
            this.labStatutIncidV.Text = "Date demande :";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(134, 108);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(110, 17);
            this.label74.TabIndex = 27;
            this.label74.Text = "Statut du ticket :";
            // 
            // labIdIncidV
            // 
            this.labIdIncidV.AutoSize = true;
            this.labIdIncidV.Location = new System.Drawing.Point(311, 67);
            this.labIdIncidV.Name = "labIdIncidV";
            this.labIdIncidV.Size = new System.Drawing.Size(109, 17);
            this.labIdIncidV.TabIndex = 26;
            this.labIdIncidV.Text = "Date demande :";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(134, 67);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(86, 17);
            this.label72.TabIndex = 25;
            this.label72.Text = "ID Incident : ";
            // 
            // labEquipIncidV
            // 
            this.labEquipIncidV.AutoSize = true;
            this.labEquipIncidV.Location = new System.Drawing.Point(311, 277);
            this.labEquipIncidV.Name = "labEquipIncidV";
            this.labEquipIncidV.Size = new System.Drawing.Size(109, 17);
            this.labEquipIncidV.TabIndex = 24;
            this.labEquipIncidV.Text = "Date demande :";
            // 
            // labDemandeurIncidV
            // 
            this.labDemandeurIncidV.AutoSize = true;
            this.labDemandeurIncidV.Location = new System.Drawing.Point(311, 230);
            this.labDemandeurIncidV.Name = "labDemandeurIncidV";
            this.labDemandeurIncidV.Size = new System.Drawing.Size(109, 17);
            this.labDemandeurIncidV.TabIndex = 23;
            this.labDemandeurIncidV.Text = "Date demande :";
            // 
            // labObjIncidV
            // 
            this.labObjIncidV.AutoSize = true;
            this.labObjIncidV.Location = new System.Drawing.Point(311, 192);
            this.labObjIncidV.Name = "labObjIncidV";
            this.labObjIncidV.Size = new System.Drawing.Size(109, 17);
            this.labObjIncidV.TabIndex = 22;
            this.labObjIncidV.Text = "Date demande :";
            // 
            // labDateDemandeIncidV
            // 
            this.labDateDemandeIncidV.AutoSize = true;
            this.labDateDemandeIncidV.Location = new System.Drawing.Point(311, 153);
            this.labDateDemandeIncidV.Name = "labDateDemandeIncidV";
            this.labDateDemandeIncidV.Size = new System.Drawing.Size(109, 17);
            this.labDateDemandeIncidV.TabIndex = 21;
            this.labDateDemandeIncidV.Text = "Date demande :";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(134, 230);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(90, 17);
            this.label73.TabIndex = 20;
            this.label73.Text = "Demandeur :";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(134, 277);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(91, 17);
            this.label75.TabIndex = 17;
            this.label75.Text = "Equipement :";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(134, 192);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(50, 17);
            this.label76.TabIndex = 16;
            this.label76.Text = "Objet :";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(134, 153);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(109, 17);
            this.label78.TabIndex = 14;
            this.label78.Text = "Date demande :";
            // 
            // dgvSuiviIncidentsV
            // 
            this.dgvSuiviIncidentsV.AllowUserToAddRows = false;
            this.dgvSuiviIncidentsV.AllowUserToDeleteRows = false;
            this.dgvSuiviIncidentsV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSuiviIncidentsV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuiviIncidentsV.Location = new System.Drawing.Point(104, 87);
            this.dgvSuiviIncidentsV.Name = "dgvSuiviIncidentsV";
            this.dgvSuiviIncidentsV.ReadOnly = true;
            this.dgvSuiviIncidentsV.RowTemplate.Height = 24;
            this.dgvSuiviIncidentsV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSuiviIncidentsV.Size = new System.Drawing.Size(1313, 143);
            this.dgvSuiviIncidentsV.TabIndex = 0;
            this.dgvSuiviIncidentsV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuiviIncidentsV_CellClick);
            // 
            // equipementV
            // 
            this.equipementV.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.equipementV.Controls.Add(this.panel8);
            this.equipementV.Controls.Add(this.groupBox18);
            this.equipementV.Controls.Add(this.groupBox19);
            this.equipementV.Controls.Add(this.dgvSuiviEquipementsV);
            this.equipementV.Location = new System.Drawing.Point(4, 25);
            this.equipementV.Name = "equipementV";
            this.equipementV.Size = new System.Drawing.Size(1548, 719);
            this.equipementV.TabIndex = 7;
            this.equipementV.Text = "Mon équipement";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel8.Controls.Add(this.label48);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(1611, 61);
            this.panel8.TabIndex = 43;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label48.Location = new System.Drawing.Point(24, 19);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(188, 28);
            this.label48.TabIndex = 19;
            this.label48.Text = "Mon équipement";
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label50);
            this.groupBox18.Controls.Add(this.clbLogiciels);
            this.groupBox18.Controls.Add(this.lvCarac);
            this.groupBox18.Controls.Add(this.label53);
            this.groupBox18.Location = new System.Drawing.Point(817, 273);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(600, 420);
            this.groupBox18.TabIndex = 10;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Caractéristiques";
            // 
            // label50
            // 
            this.label50.AllowDrop = true;
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(39, 217);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(71, 17);
            this.label50.TabIndex = 38;
            this.label50.Text = "Logiciels :";
            // 
            // clbLogiciels
            // 
            this.clbLogiciels.CheckOnClick = true;
            this.clbLogiciels.Enabled = false;
            this.clbLogiciels.FormattingEnabled = true;
            this.clbLogiciels.Location = new System.Drawing.Point(42, 246);
            this.clbLogiciels.Name = "clbLogiciels";
            this.clbLogiciels.Size = new System.Drawing.Size(527, 123);
            this.clbLogiciels.TabIndex = 37;
            // 
            // lvCarac
            // 
            this.lvCarac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvCarac.Enabled = false;
            this.lvCarac.FullRowSelect = true;
            this.lvCarac.GridLines = true;
            this.lvCarac.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvCarac.Location = new System.Drawing.Point(37, 74);
            this.lvCarac.Name = "lvCarac";
            this.lvCarac.Size = new System.Drawing.Size(532, 124);
            this.lvCarac.TabIndex = 36;
            this.lvCarac.UseCompatibleStateImageBehavior = false;
            this.lvCarac.View = System.Windows.Forms.View.Details;
            // 
            // label53
            // 
            this.label53.AllowDrop = true;
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(34, 45);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(191, 17);
            this.label53.TabIndex = 35;
            this.label53.Text = "Caractéristiques techniques :";
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.dtpDateAssignation);
            this.groupBox19.Controls.Add(this.cbMarque);
            this.groupBox19.Controls.Add(this.cbTypeEquip);
            this.groupBox19.Controls.Add(this.cbFournisseur);
            this.groupBox19.Controls.Add(this.cbEtat);
            this.groupBox19.Controls.Add(this.cbVisiteur);
            this.groupBox19.Controls.Add(this.label49);
            this.groupBox19.Controls.Add(this.label57);
            this.groupBox19.Controls.Add(this.label61);
            this.groupBox19.Controls.Add(this.label62);
            this.groupBox19.Controls.Add(this.label63);
            this.groupBox19.Controls.Add(this.label67);
            this.groupBox19.Controls.Add(this.nupGarantie);
            this.groupBox19.Controls.Add(this.label68);
            this.groupBox19.Controls.Add(this.dtpDateAchat);
            this.groupBox19.Controls.Add(this.label69);
            this.groupBox19.Controls.Add(this.LabIdEquipement);
            this.groupBox19.Controls.Add(this.label70);
            this.groupBox19.Location = new System.Drawing.Point(104, 273);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(600, 420);
            this.groupBox19.TabIndex = 9;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Détails";
            // 
            // dtpDateAssignation
            // 
            this.dtpDateAssignation.Enabled = false;
            this.dtpDateAssignation.Location = new System.Drawing.Point(296, 158);
            this.dtpDateAssignation.Name = "dtpDateAssignation";
            this.dtpDateAssignation.Size = new System.Drawing.Size(200, 22);
            this.dtpDateAssignation.TabIndex = 35;
            // 
            // cbMarque
            // 
            this.cbMarque.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbMarque.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMarque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarque.Enabled = false;
            this.cbMarque.FormattingEnabled = true;
            this.cbMarque.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.cbMarque.Location = new System.Drawing.Point(296, 381);
            this.cbMarque.Name = "cbMarque";
            this.cbMarque.Size = new System.Drawing.Size(200, 24);
            this.cbMarque.TabIndex = 34;
            // 
            // cbTypeEquip
            // 
            this.cbTypeEquip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeEquip.Enabled = false;
            this.cbTypeEquip.FormattingEnabled = true;
            this.cbTypeEquip.Location = new System.Drawing.Point(296, 336);
            this.cbTypeEquip.Name = "cbTypeEquip";
            this.cbTypeEquip.Size = new System.Drawing.Size(200, 24);
            this.cbTypeEquip.TabIndex = 33;
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFournisseur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFournisseur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFournisseur.Enabled = false;
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(296, 293);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(200, 24);
            this.cbFournisseur.TabIndex = 32;
            // 
            // cbEtat
            // 
            this.cbEtat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEtat.Enabled = false;
            this.cbEtat.FormattingEnabled = true;
            this.cbEtat.Location = new System.Drawing.Point(296, 203);
            this.cbEtat.Name = "cbEtat";
            this.cbEtat.Size = new System.Drawing.Size(200, 24);
            this.cbEtat.TabIndex = 31;
            // 
            // cbVisiteur
            // 
            this.cbVisiteur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVisiteur.Enabled = false;
            this.cbVisiteur.FormattingEnabled = true;
            this.cbVisiteur.Location = new System.Drawing.Point(296, 248);
            this.cbVisiteur.Name = "cbVisiteur";
            this.cbVisiteur.Size = new System.Drawing.Size(200, 24);
            this.cbVisiteur.TabIndex = 30;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(80, 384);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(64, 17);
            this.label49.TabIndex = 29;
            this.label49.Text = "Marque :";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(80, 339);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(48, 17);
            this.label57.TabIndex = 28;
            this.label57.Text = "Type :";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(80, 296);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(91, 17);
            this.label61.TabIndex = 27;
            this.label61.Text = "Fournisseur :";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(80, 251);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(78, 17);
            this.label62.TabIndex = 26;
            this.label62.Text = "Assigné à :";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(80, 206);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 17);
            this.label63.TabIndex = 25;
            this.label63.Text = "Etat :";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(80, 163);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(133, 17);
            this.label67.TabIndex = 24;
            this.label67.Text = "Date d\'assignation :";
            // 
            // nupGarantie
            // 
            this.nupGarantie.Enabled = false;
            this.nupGarantie.Location = new System.Drawing.Point(296, 117);
            this.nupGarantie.Name = "nupGarantie";
            this.nupGarantie.Size = new System.Drawing.Size(200, 22);
            this.nupGarantie.TabIndex = 23;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(80, 119);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(71, 17);
            this.label68.TabIndex = 22;
            this.label68.Text = "Garantie :";
            // 
            // dtpDateAchat
            // 
            this.dtpDateAchat.Enabled = false;
            this.dtpDateAchat.Location = new System.Drawing.Point(296, 69);
            this.dtpDateAchat.Name = "dtpDateAchat";
            this.dtpDateAchat.Size = new System.Drawing.Size(200, 22);
            this.dtpDateAchat.TabIndex = 21;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(80, 74);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(96, 17);
            this.label69.TabIndex = 20;
            this.label69.Text = "Date d\'achat :";
            // 
            // LabIdEquipement
            // 
            this.LabIdEquipement.AutoSize = true;
            this.LabIdEquipement.Location = new System.Drawing.Point(293, 31);
            this.LabIdEquipement.Name = "LabIdEquipement";
            this.LabIdEquipement.Size = new System.Drawing.Size(46, 17);
            this.LabIdEquipement.TabIndex = 19;
            this.LabIdEquipement.Text = "label2";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(80, 31);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(29, 17);
            this.label70.TabIndex = 18;
            this.label70.Text = "ID :";
            // 
            // dgvSuiviEquipementsV
            // 
            this.dgvSuiviEquipementsV.AllowUserToAddRows = false;
            this.dgvSuiviEquipementsV.AllowUserToDeleteRows = false;
            this.dgvSuiviEquipementsV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSuiviEquipementsV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSuiviEquipementsV.Location = new System.Drawing.Point(104, 89);
            this.dgvSuiviEquipementsV.Name = "dgvSuiviEquipementsV";
            this.dgvSuiviEquipementsV.ReadOnly = true;
            this.dgvSuiviEquipementsV.RowTemplate.Height = 24;
            this.dgvSuiviEquipementsV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSuiviEquipementsV.Size = new System.Drawing.Size(1313, 143);
            this.dgvSuiviEquipementsV.TabIndex = 7;
            this.dgvSuiviEquipementsV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSuiviEquipementsV_CellClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(58, 755);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label8.Location = new System.Drawing.Point(159, 756);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Bertrand Jacques";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label9.Location = new System.Drawing.Point(159, 815);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Technicien";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label10.Location = new System.Drawing.Point(159, 784);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(192, 17);
            this.label10.TabIndex = 4;
            this.label10.Text = "bertrand.jacques@gmail.com";
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button4.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.button4.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.button4.Location = new System.Drawing.Point(1359, 756);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(165, 35);
            this.button4.TabIndex = 8;
            this.button4.Text = "Se déconnecter";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.linkLabel1.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.MenuHighlight;
            this.linkLabel1.Location = new System.Drawing.Point(1393, 810);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(87, 22);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "A propos";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // GestionParc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1558, 853);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GestionParc";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion des équipements - Assistance GSB";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GestionParc_FormClosed);
            this.Load += new System.EventHandler(this.GestionParc_Load);
            this.tabControl1.ResumeLayout(false);
            this.gestionParcT.ResumeLayout(false);
            this.gestionParcT.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipAffecte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipNonAffecte)).EndInit();
            this.incidentsT.ResumeLayout(false);
            this.incidentsT.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIncidents)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.celluleAssistanceT.ResumeLayout(false);
            this.celluleAssistanceT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartNbIncidentMonth)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.configurationA.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.celluleAssistanceV.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.incidentsV.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuiviIncidentsV)).EndInit();
            this.equipementV.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupGarantie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSuiviEquipementsV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage gestionParcT;
        private System.Windows.Forms.TabPage incidentsT;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvEquipNonAffecte;
        private System.Windows.Forms.DataGridView dgvIncidents;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage celluleAssistanceT;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvEquipAffecte;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabPage configurationA;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.BindingSource bindingSource3;
        private System.Windows.Forms.BindingSource bindingSource4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cbFiltreStatut;
        private System.Windows.Forms.Button btnFiltreStatut;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label date_demande;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel equipement;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label duree_intervention;
        private System.Windows.Forms.Label date_intervention;
        private System.Windows.Forms.Label objet_incident;
        private System.Windows.Forms.Label demandeur_incident;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labTechn_incid;
        private System.Windows.Forms.Label labDate_interv;
        private System.Windows.Forms.Label labSolution_incid;
        private System.Windows.Forms.TextBox labSolution_incident;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label_no_resol;
        private System.Windows.Forms.Button btnResol_incident;
        private System.Windows.Forms.Label date_interv;
        private System.Windows.Forms.Label technicien_incident;
        private System.Windows.Forms.Label duree_interv;
        private System.Windows.Forms.Label labDuree_interv;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label idIncidentSelected;
        private System.Windows.Forms.TextBox tbPrenomA;
        private System.Windows.Forms.TextBox tbNomA;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox cbProfils;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbPasswordA;
        private System.Windows.Forms.TextBox tbMailA;
        private System.Windows.Forms.Button btnSaveTypeComposants;
        private System.Windows.Forms.TextBox tbTypeComposA;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnSaveTypeEquipements;
        private System.Windows.Forms.TextBox tbTypeEquipements;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnSaveFournisseurs;
        private System.Windows.Forms.TextBox tbAdrFournissA;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tbNomFournissA;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnSaveUtilisateur;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnSaveUniteComposants;
        private System.Windows.Forms.TextBox tbUniteComposA;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button btnSaveStatutIncidents;
        private System.Windows.Forms.TextBox tbStatutIncidA;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btnSaveProfils;
        private System.Windows.Forms.TextBox tbProfilA;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnSaveNiveauIncidents;
        private System.Windows.Forms.TextBox tbNivIncidA;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btnSaveMarques;
        private System.Windows.Forms.TextBox tbMarque;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnSaveEtatEquipements;
        private System.Windows.Forms.TextBox tbEtatEquipA;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnSaveLogicielEquipements;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbMailFournissA;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbTelFournissA;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label nbVisiteurs;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TabPage incidentsV;
        private System.Windows.Forms.TabPage celluleAssistanceV;
        private System.Windows.Forms.TabPage equipementV;
        private System.Windows.Forms.Button btnFiltreNiveau;
        private System.Windows.Forms.ComboBox cbfiltreNiveau;
        private System.Windows.Forms.GroupBox gbCacheDetails;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.ListBox lbEquipements;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label labNbEquipV;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label labNbIncidAttenteV;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label labNbIncidResolV;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label labNbDemandesIncidV;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView dgvSuiviIncidentsV;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.DataGridView dgvSuiviEquipementsV;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.CheckedListBox clbLogiciels;
        private System.Windows.Forms.ListView lvCarac;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label dureeMoyResol;
        private System.Windows.Forms.Label nbIncidentsResolus;
        private System.Windows.Forms.Label nbIncidents;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label nbEquipementsAffectes;
        private System.Windows.Forms.Label nbEquipements;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label nbIncidentsResolusMonth;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label nbEquipAffectesMonth;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.DateTimePicker dtpDateAssignation;
        private System.Windows.Forms.ComboBox cbMarque;
        private System.Windows.Forms.ComboBox cbTypeEquip;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.ComboBox cbEtat;
        private System.Windows.Forms.ComboBox cbVisiteur;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.NumericUpDown nupGarantie;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.DateTimePicker dtpDateAchat;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label LabIdEquipement;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label labEquipIncidV;
        private System.Windows.Forms.Label labDemandeurIncidV;
        private System.Windows.Forms.Label labObjIncidV;
        private System.Windows.Forms.Label labDateDemandeIncidV;
        private System.Windows.Forms.Label labIdIncidV;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label labStatutIncidV;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label labDateResolIncidV;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label labTechnicIncidV;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.TextBox tbResolIncidV;
        private System.Windows.Forms.Button btnExportPDF;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbMonthChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartNbIncidentMonth;
        private System.Windows.Forms.ComboBox cbYearChart;
        private System.Windows.Forms.Button btnValiderStatsGraph;
        private System.Windows.Forms.Button button3;
    }
}

