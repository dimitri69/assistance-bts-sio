﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Security.Cryptography;
using System.Windows.Forms.DataVisualization.Charting;

namespace TestGSB1
{
    public partial class GestionParc : Form
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// VARIABLES PRIVEES ET DECLARATION
        DataSet dsEquipAffectes = new DataSet();
        DataSet dsEquipNonAffectes = new DataSet();
        DataSet dsIncidents = new DataSet();
        DataSet dsSuiviIncidentsV = new DataSet();
        DataSet dsSuiviEquipementsV = new DataSet();  

        private String id_utilisateur;
        private String role_utilisateur;
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private String listeEquipementSQL = "SELECT eq.id as 'ID', ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie (an)', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is null";
        private String listeEquipementAffectesSQL = "SELECT eq.id, ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie', eq.date_assignation as 'Assigné le', ut.prenom+' '+ut.nom as 'Assigné à', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is not null";
        private String listeDemandesIncidentsSQL = "SELECT i.id as 'Id', date_demande as 'Date demande', objet as 'Objet', date_modification as 'Dernière modification', us.prenom+' '+us.nom as 'Demandeur', st.libelle as 'Statut', i.id_statuts, id_utilisateurs_1, id_niveaux FROM incidents i LEFT JOIN statuts st ON st.id = i.id_statuts LEFT JOIN utilisateurs us ON us.id = i.id_utilisateurs";
        private String listeMesEquipementSQL = "SELECT eq.id as 'ID', ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie (an)', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs = ";
        private String listeMesIncidentsSQL = "SELECT i.id as 'Id', date_demande as 'Date demande', objet as 'Objet', date_modification as 'Dernière modification', us.prenom+' '+us.nom as 'Demandeur', st.libelle as 'Statut', i.id_statuts, id_utilisateurs_1, id_niveaux FROM incidents i LEFT JOIN statuts st ON st.id = i.id_statuts LEFT JOIN utilisateurs us ON us.id = i.id_utilisateurs WHERE i.id_utilisateurs = ";

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Appelé à la création du formulaire (la fenêtre principale)
        public GestionParc(String id, String name, String mail, String role, String avatar)
        {
            InitializeComponent();
            InitializeListView();

            // Remplissage de la page "Cellule d'Assistance"
            Statistiques stat = new Statistiques(id_utilisateur);
            nbVisiteurs.Text = stat.getNbVisiteurs();

            // Remplissage des Combobox de filtre
            cbFiltreStatut.Items.Add(new ComboBoxItem("Par statut", "0"));
            cbFiltreStatut.Items.Add(new ComboBoxItem("Tous les incidents", "1"));
            cbFiltreStatut.Items.Add(new ComboBoxItem("Demandes d'incidents", "2"));
            cbFiltreStatut.Items.Add(new ComboBoxItem("Incidents Résolus", "3"));
            cbFiltreStatut.Items.Add(new ComboBoxItem("Mes incidents", "4"));
            cbFiltreStatut.Text = "Par statut";
            
            cbfiltreNiveau.Items.Add(new ComboBoxItem("Par niveau", "0"));
            cbfiltreNiveau.Items.Add(new ComboBoxItem("Demande basique", "1"));
            cbfiltreNiveau.Items.Add(new ComboBoxItem("Intervention à distance", "2"));
            cbfiltreNiveau.Items.Add(new ComboBoxItem("Installation matérielle/lourde", "3"));
            cbfiltreNiveau.Items.Add(new ComboBoxItem("Site Web Compte-rendus/frais de visites", "4"));
            cbfiltreNiveau.Text = "Par niveau";

            cbMonthChart.Items.Add(new ComboBoxItem("Janvier", "1"));
            cbMonthChart.Items.Add(new ComboBoxItem("Février", "2"));
            cbMonthChart.Items.Add(new ComboBoxItem("Mars", "3"));
            cbMonthChart.Items.Add(new ComboBoxItem("Avril", "4"));
            cbMonthChart.Items.Add(new ComboBoxItem("Mai", "5"));
            cbMonthChart.Text = "Mars";

            cbYearChart.Items.Add(new ComboBoxItem("2015", "2015"));
            cbYearChart.Items.Add(new ComboBoxItem("2016", "2016"));
            cbYearChart.Items.Add(new ComboBoxItem("2017", "2017"));
            cbYearChart.Text = "2016";

            // Affichage des infos de l'utilisateur connecté
            id_utilisateur = id;
            label8.Text = name + " ( ID " + id + " )";
            label10.Text = mail;
            label9.Text = role;
            role_utilisateur = role;
            if (role != "3") // Admin
            {
                this.tabControl1.TabPages.Remove(configurationA);
            }
            if (role != "2") // Technicien
            {
                this.tabControl1.TabPages.Remove(celluleAssistanceT);
                this.tabControl1.TabPages.Remove(incidentsT);
                this.tabControl1.TabPages.Remove(gestionParcT);
            }
            if (role != "1")    // Visiteur
            {
                this.tabControl1.TabPages.Remove(equipementV);
                this.tabControl1.TabPages.Remove(incidentsV);
                this.tabControl1.TabPages.Remove(celluleAssistanceV);
            }
            if (role == "3") // Admin
            {                
                this.tabControl1.TabPages.Add(celluleAssistanceT);
                this.tabControl1.TabPages.Add(incidentsT);
                this.tabControl1.TabPages.Add(gestionParcT);
            }
            GetData(listeEquipementSQL, connectionString);
            GetData2(listeEquipementAffectesSQL, connectionString);
        }

        public GestionParc()
        {
            InitializeComponent();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // CHARGEMENT DES DONNEES DU FORMULAIRE PRINCIPAL
        private void GestionParc_Load(object sender, EventArgs e)
        {
            InitializeDataGridView();

            // Liste des équipements - Récupération des données de la BDD 
            String equipementSQL = listeEquipementSQL;            
            GetData(equipementSQL, connectionString);
            // Ajout du bouton Affecter dans une colonne de la GridView1
            DataGridViewButtonColumn btn = new DataGridViewButtonColumn();
            dgvEquipNonAffecte.Columns.Add(btn);
            btn.HeaderText = "Action";
            btn.Text = "Affecter"; 
            btn.Name = "btn_affect";
            btn.UseColumnTextForButtonValue = true;

            // Liste des équipements affectés - Récupération des données de la BDD 
            String equipementSQL2 = listeEquipementAffectesSQL;            
            GetData2(equipementSQL2, connectionString);
            // Ajout du bouton Affecter dans une colonne de la GridView4
            DataGridViewButtonColumn btn2 = new DataGridViewButtonColumn();
            dgvEquipAffecte.Columns.Add(btn2);
            btn2.HeaderText = "Action";
            btn2.Text = "Désaffecter";
            btn2.Name = "btn_desaffect";
            btn2.UseColumnTextForButtonValue = true;

            // Liste des incidents - Récupération des données de la BDD 
            String incident1SQL2 = listeDemandesIncidentsSQL;            
            GetData3(incident1SQL2, connectionString);

            String equipementSQL3 = listeMesEquipementSQL + id_utilisateur;
            GetData4(equipementSQL3, connectionString);

            String incidents2SQL2 = listeMesIncidentsSQL + id_utilisateur;
            GetData5(incidents2SQL2, connectionString);

            // Remplissage de la Combobox comboBox4 : Fournisseur de l'équipement
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String fournisseurs_equipSQL = "SELECT ty.libelle+' '+ma.nom as 'equipement' FROM equipements eq LEFT JOIN marques ma ON ma.id = eq.id_marques LEFT JOIN types ty ON ty.id = eq.id_types WHERE id_utilisateurs = " + id_utilisateur;
            SqlCommand fournisseurs_equipComm = new SqlCommand(fournisseurs_equipSQL, conn);
            SqlDataReader dr4 = fournisseurs_equipComm.ExecuteReader();
            if (dr4.HasRows)
            {
                while (dr4.Read())
                {
                    lbEquipements.Items.Add(dr4["equipement"].ToString());
                }
            }
            dr4.Close();            

            // Stats Tableau de bord Visiteur
            Statistiques stats = new Statistiques(id_utilisateur);
            labNbDemandesIncidV.Text = stats.getNbMesIncidentsResolus(id_utilisateur) + " demande(s) d'incidents";
            labNbIncidResolV.Text = "- " +stats.getNbMesIncidentsResolus(id_utilisateur) + " incident(s) résolus";
            labNbIncidAttenteV.Text = "- "+stats.getNbMesIncidentsEnAttente(id_utilisateur) + " incident(s) en attente";
            labNbEquipV.Text = stats.getNbMesEquipements(id_utilisateur) + " équipement(s)";
            
            // Stats Statistiques Technicien & Admin
            nbVisiteurs.Text = stats.getNbVisiteurs();
            nbEquipements.Text = stats.getNbEquipements();
            nbEquipementsAffectes.Text = stats.getNbEquipementsAffectes();
            nbEquipAffectesMonth.Text = stats.getNbEquipementsOfMonth();
            nbIncidents.Text = stats.getNbIncidents();
            nbIncidentsResolus.Text = stats.getNbIncidentsResolus();
            dureeMoyResol.Text = stats.getTmpsMoyDemande().ToString()+" jour(s)";
            nbIncidentsResolusMonth.Text = stats.getNbIncidentsOfMonth();

            // Liste des profils                        
            String equipSQL = "SELECT id, libelle FROM profils";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbProfils.Items.Add(new ComboBoxItem(dr["libelle"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();                 
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion du parc - Ajout d'un équipement
        private void button1_Click(object sender, EventArgs e)
        {
            AjoutEquipement frm_aj_equip = new AjoutEquipement(id_utilisateur);
            frm_aj_equip.Show();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion du parc - Affectation de l'équipement    
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // Quand on clique sur une ligne, ça ouvre une nouvelle forme (la form affecterEquipement)
            // qui sert à affecter l'quipement sélectionner à une nouvelle personne
            if (e.ColumnIndex == 6)//Column index
            {
                if (dgvEquipNonAffecte.Rows[dgvEquipNonAffecte.CurrentCell.RowIndex] != null)
                {                    
                    int i = dgvEquipNonAffecte.CurrentCell.RowIndex; // Numéro de la ligne
                    int id_equipement = (int)dgvEquipNonAffecte.Rows[i].Cells[0].Value; // id de l'équipement
                    String Type = (string)dgvEquipNonAffecte.Rows[i].Cells[1].Value; // Type
                    String Marque = (string)dgvEquipNonAffecte.Rows[i].Cells[2].Value; // Marque
                    String equip_select = "";
                    equip_select += Type + " - " + Marque;  // Pour afficher le nom du matériel à affecter sur la Form d'affectation                   

                    AffecterEquipement frm_aff_equip = new AffecterEquipement(equip_select, id_utilisateur);
                    frm_aff_equip.Id_equipement = id_equipement.ToString();
                    frm_aff_equip.Show();
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents - Clic Bouton Nouvelle demande d'incident
        private void button2_Click(object sender, EventArgs e)
        {
            DemandeIncident frm_dem_equip = new DemandeIncident(id_utilisateur);
            frm_dem_equip.Show();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents - Clic sur une ligne de la liste d'incidents        
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Réaffichage de tous les champs et labels
            labDate_interv.Show();
            labSolution_incid.Show();
            labTechn_incid.Show();
            labSolution_incident.Show();
            labDuree_interv.Show();
            label_no_resol.Show();
            btnResol_incident.Show();

            int IdxLigneActuelle = e.RowIndex;
            String incidentID = "";
            if (IdxLigneActuelle > -1)  //Si c'est pas la ligne du tri
            {
                DataGridViewRow ligne = dgvIncidents.Rows[IdxLigneActuelle];
                incidentID = ligne.Cells[0].Value.ToString();                

                // Connection à la BDD
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                // Remplissage du cadre de l'incident
                String incident_selectedSQL = "SELECT i.id as 'id_incident', st.libelle as 'statut', i.date_demande, i.objet, eq.id as 'id_equipement', ty.libelle+' '+ma.nom as 'equipement', i.date_intervention, i.solution, i.duree_intevrention, i.date_modification, ut.prenom+' '+ut.nom as 'demandeur', ut2.prenom+' '+ut2.nom as 'technicien' FROM incidents i LEFT JOIN  utilisateurs ut ON ut.id = i.id_utilisateurs LEFT JOIN equipements eq ON eq.id = i.id_equipements LEFT JOIN statuts st ON st.id = i.id_statuts LEFT JOIN niveaux niv ON niv.id = i.id_niveaux LEFT JOIN utilisateurs ut2 ON ut2.id = i.id_utilisateurs_1 LEFT JOIN marques ma ON ma.id = eq.id_marques LEFT JOIN types ty ON ty.id = eq.id_types WHERE i.id = " + incidentID;
                SqlCommand incident_selectedComm = new SqlCommand(incident_selectedSQL, conn);
                SqlDataReader dr = incident_selectedComm.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        idIncidentSelected.Text = incidentID;
                        groupBox4.Text = "[" + dr["statut"] + "] Incident n°" + incidentID;
                        objet_incident.Text = dr["objet"].ToString();
                        date_demande.Text = dr["date_demande"].ToString();
                        demandeur_incident.Text = dr["demandeur"].ToString();
                        equipement.Text = dr["equipement"].ToString();
                        objet_incident.Text = dr["objet"].ToString();
                        labSolution_incident.Text = dr["solution"].ToString();
                        gbCacheDetails.Hide();
                        if (dr["solution"].ToString() == "" && dr["date_intervention"].ToString() == "")
                        {
                            labDate_interv.Hide();
                            labSolution_incid.Hide();
                            labTechn_incid.Hide();
                            labSolution_incident.Hide();
                            labDuree_interv.Hide();
                        }
                        else
                        {
                            label_no_resol.Hide();
                            btnResol_incident.Hide();
                        }
                    }
                }
                dr.Close();
            }
            else
            {
                gbCacheDetails.Show();
            }
            
        }             

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents - Bouton de résolution d'un incident
        private void btnResol_incident_Click(object sender, EventArgs e)
        {
            String id_incident = idIncidentSelected.Text;
            ResolIncident ri = new ResolIncident(id_incident.ToString(), id_utilisateur);
            ri.Show();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion de parc - Clic sur une ligne d'équipement pour l'éditer
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 6)
            {
                int IdxLigneActuelle = e.RowIndex;                
                String equipementID = "";
                if (IdxLigneActuelle > -1)  // On prends pas en compte les headers des colonnes
                {
                    DataGridViewRow ligne = dgvEquipNonAffecte.Rows[IdxLigneActuelle];
                    equipementID = ligne.Cells[0].Value.ToString();
                    EditionEquipement ee = new EditionEquipement(equipementID, id_utilisateur);
                    ee.Show();
                }
            }           
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents - Filtre sur le niveau des incidents
        private void btnFiltreNiveau_Click(object sender, EventArgs e)
        {
            String conditions = "";
            String order = "";
            if (((ComboBoxItem)cbfiltreNiveau.SelectedItem).HiddenValue == "1")
            {
                order = "id_niveaux";
                conditions = "id_niveaux = '1'";
            }
            else if (((ComboBoxItem)cbfiltreNiveau.SelectedItem).HiddenValue == "2")
            {
                order = "id_niveaux";
                conditions = "id_niveaux = '2'";
            }
            else if (((ComboBoxItem)cbfiltreNiveau.SelectedItem).HiddenValue == "3")
            {
                order = "id_niveaux";
                conditions = "id_niveaux = '3'";
            }
            else if (((ComboBoxItem)cbfiltreNiveau.SelectedItem).HiddenValue == "4")
            {
                order = "id_niveaux";
                conditions = "id_niveaux = '4'";
            }
            MessageBox.Show(listeDemandesIncidentsSQL + conditions);

            DataView dv;
            dv = new DataView(dsIncidents.Tables[0], conditions, order + " Desc", DataViewRowState.CurrentRows);            
            dgvIncidents.DataSource = dv;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion des incidents - Filtre des incidents par statut
        private void button5_Click(object sender, EventArgs e)
        {
            String conditions = "";
            String order = "";
            if (((ComboBoxItem)cbFiltreStatut.SelectedItem).HiddenValue == "1")
            {
                order = "id_statuts";
                conditions = "1=1";
            }
            else if (((ComboBoxItem)cbFiltreStatut.SelectedItem).HiddenValue == "2")
            {
                order = "id_statuts";
                conditions = "id_statuts = '1'";
            }
            else if (((ComboBoxItem)cbFiltreStatut.SelectedItem).HiddenValue == "3")
            {
                order = "id_statuts";
                conditions = "id_statuts = '2'";
            }
            else if (((ComboBoxItem)cbFiltreStatut.SelectedItem).HiddenValue == "4")
            {
                order = "id_statuts";
                conditions = "id_utilisateurs_1 = '" + id_utilisateur + "'";
            }
            MessageBox.Show(listeDemandesIncidentsSQL + conditions);

            DataView dv;
            dv = new DataView(dsIncidents.Tables[0], conditions, order + " Desc", DataViewRowState.CurrentRows);
            dgvIncidents.DataSource = dv;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion de Parc - Clic sur une ligne d'équipement
        private void dgvEquipAffecte_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int IdxLigneActuelle = e.RowIndex;
            String equipementID = "";
            // Quand on sélectionne la ligne
            if (e.ColumnIndex != 8)
            {                
                if (IdxLigneActuelle > -1)  // On prends pas en compte les headers des colonnes
                {
                    DataGridViewRow ligne = dgvEquipAffecte.Rows[IdxLigneActuelle];
                    equipementID = ligne.Cells[0].Value.ToString();

                    EditionEquipement ee = new EditionEquipement(equipementID, id_utilisateur);
                    ee.Show();
                }
            }
            // Bouton Désaffecter
            if (e.ColumnIndex == 8)
            {
                DataGridViewRow ligne = dgvEquipAffecte.Rows[IdxLigneActuelle];
                equipementID = ligne.Cells[0].Value.ToString();
                desaffecterMateriel(equipementID);
            }
        }
        private void desaffecterMateriel(string id_equipement)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String upd_affectSQL = "UPDATE equipements SET id_utilisateurs = NULL WHERE id = " + id_equipement;
            SqlCommand upd_affectComm = new SqlCommand(upd_affectSQL, conn);
            int rowsAffected = upd_affectComm.ExecuteNonQuery();
            if (rowsAffected > 0)
            {
                MessageBox.Show("L'équipement a été remis en stock.", "Désaffecter l'équipement", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            conn.Close();
            GetData(listeEquipementSQL, connectionString);
            GetData2(listeEquipementAffectesSQL, connectionString);
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Détails Equipement Visiteur - Clic sur une ligne d'équipement
        private void dgvSuiviEquipementsV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int IdxLigneActuelle = e.RowIndex;
            String equipementID = "";
            // Quand on sélectionne la ligne
            if (IdxLigneActuelle > -1)  // On prends pas en compte les headers des colonnes
            {
                DataGridViewRow ligne = dgvSuiviEquipementsV.Rows[IdxLigneActuelle];
                equipementID = ligne.Cells[0].Value.ToString();                
                afficheDetailsEquipV(equipementID);
            }            
        }
        private void afficheDetailsEquipV(String IdEquipement)
        {
            // Connection à la BDD
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            // Remplissage de la Combobox comboBox1 : Type de l'équipement
            String types_equipSQL = "SELECT id as id, libelle as libelle FROM types";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbTypeEquip.Items.Add(new ComboBoxItem(dr["libelle"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();

            // Remplissage de la Combobox comboBox3 : Etat de l'équipement
            String etats_equipSQL = "SELECT id as id, libelle as libelle FROM etats";
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    cbEtat.Items.Add(new ComboBoxItem(dr2["libelle"].ToString(), dr2["id"].ToString()));
                }
            }
            dr2.Close();

            // Remplissage de la Combobox comboBox3 : Marque de l'équipement
            String marques_equipSQL = "SELECT id as id, nom as nom FROM marques";
            SqlCommand marques_equipComm = new SqlCommand(marques_equipSQL, conn);
            SqlDataReader dr3 = marques_equipComm.ExecuteReader();
            if (dr3.HasRows)
            {
                while (dr3.Read())
                {
                    cbMarque.Items.Add(new ComboBoxItem(dr3["nom"].ToString(), dr3["id"].ToString()));
                }
            }
            dr3.Close();

            // Remplissage de la Combobox comboBox4 : Fournisseur de l'équipement
            String fournisseurs_equipSQL = "SELECT id as id, nom as nom FROM fournisseurs";
            SqlCommand fournisseurs_equipComm = new SqlCommand(fournisseurs_equipSQL, conn);
            SqlDataReader dr4 = fournisseurs_equipComm.ExecuteReader();
            if (dr4.HasRows)
            {
                while (dr4.Read())
                {
                    cbFournisseur.Items.Add(new ComboBoxItem(dr4["nom"].ToString(), dr4["id"].ToString()));
                }
            }
            dr4.Close();

            string assigneaSQL = "select id, prenom+' '+nom as 'visiteur' from utilisateurs";
            SqlCommand assigneaComm = new SqlCommand(assigneaSQL, conn);
            SqlDataReader dr7 = assigneaComm.ExecuteReader();
            if (dr7.HasRows)
            {
                while (dr7.Read())
                {
                    cbVisiteur.Items.Add(new ComboBoxItem(dr7["visiteur"].ToString(), dr7["id"].ToString()));
                }
            }
            dr7.Close();

            clbLogiciels.Items.Clear();
            string logicielsSQL = "select id, nom+' '+version+' ('+editeur+')' as 'Caracteristique' from logiciels";
            SqlCommand logicielsComm = new SqlCommand(logicielsSQL, conn);
            SqlDataReader dr8 = logicielsComm.ExecuteReader();
            if (dr8.HasRows)
            {
                while (dr8.Read())
                {
                    clbLogiciels.Items.Add(new ComboBoxItem(dr8["Caracteristique"].ToString(), dr8["id"].ToString()));
                }
            }
            dr8.Close();

            var dateToConvert = "Wed Oct 02 2013 00:00:00 GMT+0100 (GMT Daylight Time)";
            var format = "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz '(GMT Daylight Time)'";

            String equipementSQL = "SELECT date_achat as 'date_achat', garantie as 'garantie', date_assignation, date_modification, id_etats, id_utilisateurs, id_fournisseurs, id_types, id_marques, et.libelle as etat, ma.nom as marque, fo.nom as fournisseur, ty.libelle as type_eq, ut.prenom+' '+ut.nom as proprietaire FROM equipements eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques LEFT JOIN etats et ON et.id = eq.id_etats  where eq.id = " + IdEquipement;
            SqlCommand equipementComm = new SqlCommand(equipementSQL, conn);
            SqlDataReader dr9 = equipementComm.ExecuteReader();
            dr9.Read();

            LabIdEquipement.Text = IdEquipement;
            DateTime dateAchat = Convert.ToDateTime(dr9["date_achat"]);
            dtpDateAchat.Value = dateAchat;
            nupGarantie.Text = dr9["garantie"].ToString();
            DateTime dateAssignation = Convert.ToDateTime(dr9["date_achat"]);
            dtpDateAssignation.Value = dateAssignation;
            cbEtat.Text = dr9["etat"].ToString();
            cbVisiteur.Text = dr9["proprietaire"].ToString();
            cbFournisseur.Text = dr9["fournisseur"].ToString();
            cbTypeEquip.Text = dr9["type_eq"].ToString();
            cbMarque.Text = dr9["marque"].ToString();
            dr9.Close();

            String logiciels_equipSQL = "SELECT nom+' '+version+' ('+editeur+')' as logiciels_checked FROM logiciel_equipement le LEFT JOIN logiciels lo ON lo.id = le.id_logiciels where le.id_equipements = " + IdEquipement;
            SqlCommand logiciels_equipComm = new SqlCommand(logiciels_equipSQL, conn);
            SqlDataReader dr10 = logiciels_equipComm.ExecuteReader();
            if (dr10.HasRows)
            {               
                while (dr10.Read())
                {                    
                    for (int i = 0; i <= (clbLogiciels.Items.Count - 1); i++)
                    {
                        String ItemSelected = dr10["logiciels_checked"].ToString();
                        if (ItemSelected == clbLogiciels.Items[i].ToString())
                        {
                            clbLogiciels.SetItemChecked(i, true);
                        }
                    }

                }
            }
            dr10.Close();

            lvCarac.Items.Clear();
            String carac_equipSQL = "SELECT tm.libelle as carac_type, CONVERT(VARCHAR(80),valeur) as valeur, um.libelle as unite FROM caracteristique_equipement ce LEFT JOIN caracteristiques ca ON ca.id = ce.id_caracteristiques LEFT JOIN unites_materiels um ON um.id = ca.id_unites_materiels LEFT JOIN type_materiels tm ON tm.id = ca.id_type_materiels WHERE ce.id_equipements = " + IdEquipement;
            SqlCommand carac_equipComm = new SqlCommand(carac_equipSQL, conn);
            SqlDataReader dr11 = carac_equipComm.ExecuteReader();
            if (dr11.HasRows)
            {
                while (dr11.Read())
                {
                    string[] row = { dr11["carac_type"].ToString(), dr11["valeur"].ToString(), dr11["unite"].ToString() };
                    ListViewItem itm;
                    itm = new ListViewItem(row);
                    lvCarac.Items.Add(itm);
                }
            }
            dr11.Close();

            conn.Close();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Détails Incidents Visiteur - Clic sur une ligne d'incident
        private void dgvSuiviIncidentsV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int IdxLigneActuelle = e.RowIndex;
            String incidentID = "";
            if (IdxLigneActuelle > -1)  //Si c'est pas la ligne du tri
            {
                DataGridViewRow ligne = dgvSuiviIncidentsV.Rows[IdxLigneActuelle];
                incidentID = ligne.Cells[0].Value.ToString();

                // Connection à la BDD
                SqlConnection conn = new SqlConnection(connectionString);
                conn.Open();

                // Remplissage du cadre de l'incident
                String incident_selectedSQL = "SELECT i.id as 'id_incident', st.libelle as 'statut', i.date_demande, i.objet, eq.id as 'id_equipement', ty.libelle+' '+ma.nom as 'equipement', i.date_intervention as date_intervention, i.solution, i.duree_intevrention, i.date_modification, ut.prenom+' '+ut.nom as 'demandeur', ut2.prenom+' '+ut2.nom as 'technicien' FROM incidents i LEFT JOIN  utilisateurs ut ON ut.id = i.id_utilisateurs LEFT JOIN equipements eq ON eq.id = i.id_equipements LEFT JOIN statuts st ON st.id = i.id_statuts LEFT JOIN niveaux niv ON niv.id = i.id_niveaux LEFT JOIN utilisateurs ut2 ON ut2.id = i.id_utilisateurs_1 LEFT JOIN marques ma ON ma.id = eq.id_marques LEFT JOIN types ty ON ty.id = eq.id_types WHERE i.id = " + incidentID;
                SqlCommand incident_selectedComm = new SqlCommand(incident_selectedSQL, conn);
                SqlDataReader dr = incident_selectedComm.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        labIdIncidV.Text = incidentID;
                        labStatutIncidV.Text = dr["statut"].ToString();
                        labObjIncidV.Text = dr["objet"].ToString();
                        labDateDemandeIncidV.Text = dr["date_demande"].ToString();
                        labDemandeurIncidV.Text = dr["demandeur"].ToString();
                        labEquipIncidV.Text = dr["equipement"].ToString();
                        if (dr["solution"].ToString() == "")
                        {
                            labTechn_incid.Hide();
                            labDateResolIncidV.Hide();
                            labSolution_incid.Hide();
                        }
                        else
                        {
                            labTechn_incid.Text = dr["technicien"].ToString();
                            labDateResolIncidV.Text = dr["date_intervention"].ToString();
                            tbResolIncidV.Text = dr["solution"].ToString();
                        }
                    }
                }
                dr.Close();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Connexion - Inscription - Fonction de hashage en MD5
        public string ConvertMD5(string value)
        {
            MD5 md = MD5.Create();
            byte[] data = md.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Configuration Administrateur - Enregistrement des formulaires d'ajout
        private void btnSaveUtilisateur_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO utilisateurs (nom, prenom, adresse_mail, password, id_profils) VALUES(@nom, @prenom, @adresse_mail, @password, @id_profils)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@nom", tbNomA.Text);
            InsertAComm.Parameters.AddWithValue("@prenom", tbPrenomA.Text);
            InsertAComm.Parameters.AddWithValue("@adresse_mail", tbMailA.Text);
            String password = tbPasswordA.Text.ToString();
            InsertAComm.Parameters.AddWithValue("@password", ConvertMD5(password));
            InsertAComm.Parameters.AddWithValue("@id_profils", ((ComboBoxItem)cbProfils.SelectedItem).HiddenValue);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("L'utilisateur a été ajouté avec succès.", "Utilisateur ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveProfils_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO profils (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbProfilA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le profil utilisateur a été ajouté avec succès.", "Profil utilisateur ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveFournisseurs_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO fournisseurs (nom, adresse, tel, mail) VALUES(@nom, @adresse, @tel, @mail)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@nom", tbNomFournissA.Text);
            InsertAComm.Parameters.AddWithValue("@adresse", tbAdrFournissA.Text);
            InsertAComm.Parameters.AddWithValue("@tel", tbTelFournissA.Text);
            InsertAComm.Parameters.AddWithValue("@mail", tbMailFournissA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le fournisseur a été ajouté avec succès.", "Fournisseur ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveEtatEquipements_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO etats (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbEtatEquipA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("L'état d'équipement a été ajouté avec succès.", "Etat d'équipement ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveTypeComposants_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO type_materiels (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbTypeComposA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le type de composant a été ajouté avec succès.", "Type de composant ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveUniteComposants_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO unites_materiels (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbUniteComposA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le'unité de composant a été ajouté avec succès.", "Unité composant ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveStatutIncidents_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO statuts (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbStatutIncidA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le statut d'incident a été ajouté avec succès.", "Statut d'incident ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveNiveauIncidents_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String insertASQL = "INSERT INTO niveaux (libelle) VALUES(@libelle)";
            SqlCommand InsertAComm = new SqlCommand(insertASQL, conn);
            InsertAComm.Parameters.AddWithValue("@libelle", tbNivIncidA.Text);
            int rowsAffected = InsertAComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le niveau d'incident a été ajouté avec succès.", "Niveau d'incident ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveTypeEquipements_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            String insSQL = "INSERT INTO types (libelle) VALUES(@type_equipement)";
            SqlCommand insComm = new SqlCommand(insSQL, conn);
            insComm.Parameters.AddWithValue("@type_equipement", tbTypeEquipements.Text.ToString());
            conn.Open();
            int rowsAffected = insComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Le type d'équipement a été ajouté avec succès.", "Type d'équipement ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }
        private void btnSaveMarques_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            String insSQL = "INSERT INTO marques (nom) VALUES(@marque)";
            SqlCommand insComm = new SqlCommand(insSQL, conn);
            insComm.Parameters.AddWithValue("@marque", tbMarque.Text.ToString());
            conn.Open();
            int rowsAffected = insComm.ExecuteNonQuery();
            conn.Close();
            if (rowsAffected > 0)
            {
                MessageBox.Show("La marque a été ajoutée avec succès.", "Marque ajoutée.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tbMarque.Clear();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Gestion de parc - Export en PDF
        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(dgvEquipAffecte.ColumnCount);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 80;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;

            //Adding Header row
            foreach (DataGridViewColumn column in dgvEquipAffecte.Columns)
            {
                if(column.Index < 8)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                    pdfTable.AddCell(cell);
                }
                else
                {
                    PdfPCell cell8 = new PdfPCell(new Phrase("Statut"));
                    cell8.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                    pdfTable.AddCell(cell8);
                }                
            }

            //Adding DataRow
            foreach (DataGridViewRow row in dgvEquipAffecte.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfTable.AddCell(cell.Value.ToString());
                }
            }

            PdfPTable pdfTable2 = new PdfPTable(dgvEquipNonAffecte.ColumnCount+2);
            pdfTable2.DefaultCell.Padding = 3;
            pdfTable2.WidthPercentage = 80;
            pdfTable2.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable2.DefaultCell.BorderWidth = 1;

            //Adding DataRow
            foreach (DataGridViewRow row in dgvEquipNonAffecte.Rows)
            {                
                foreach (DataGridViewCell cell in row.Cells)
                {
                    String cellule6 = cell.ColumnIndex.Equals(6).ToString();
                    String cellule7 = cell.ColumnIndex.Equals(7).ToString();
                    if (cell.ColumnIndex == 5)
                    {
                        PdfPCell cell2 = new PdfPCell(new Phrase("/"));
                        pdfTable2.AddCell(cell2);
                        PdfPCell cell3 = new PdfPCell(new Phrase("/"));
                        pdfTable2.AddCell(cell3);                       
                    }
                    pdfTable2.AddCell(cell.Value.ToString());
                }                
            }

            //Exporting to PDF
            string folderPath = "C:\\Users\\Dimitri\\Documents\\BTS SIO\\Année2\\PPE\\Projet2-\\Ressources_Epreuve\\Export_ParcInformation\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            DateTime date_now = DateTime.Now;
            String filePath = "Export_ParcInfo" + date_now.Day.ToString() + date_now.Month.ToString() + date_now.Year.ToString() + ".pdf";
            using (FileStream stream = new FileStream(folderPath + filePath, FileMode.Create))
            {
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                pdfDoc.Add(pdfTable);
                pdfDoc.Add(pdfTable2);
                pdfDoc.Close();
                stream.Close();
                MessageBox.Show("Export terminé.");
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Cellule Assistance - Graphique stats
        /*private void cbYearChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Graphique nombre incidents mois
            chartNbIncidentMonth.Series["Series1"].XValueType = ChartValueType.Auto;            

            String date_chart = ((ComboBoxItem)cbMonthChart.SelectedItem).HiddenValue.ToString()+((ComboBoxItem)cbYearChart.SelectedItem).HiddenValue.ToString();
            String chartNbMoisSQL = "SELECT CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) as jour, COUNT(*) as nb_incident FROM incidents WHERE CONVERT(varchar(50),MONTH(date_demande))+CONVERT(varchar(50),YEAR(date_demande)) = " + date_chart + " GROUP BY CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) ";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand chartNbMoisComm = new SqlCommand(chartNbMoisSQL, conn);
            SqlDataReader dr10 = chartNbMoisComm.ExecuteReader();
            if (dr10.HasRows)
            {
                while (dr10.Read())
                {
                    chartNbIncidentMonth.Series["Series1"].Points.AddXY(dr10["jour"], dr10["nb_incident"]);
                }
            }
            chartNbIncidentMonth.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chartNbIncidentMonth.Series["Series1"].BorderWidth = 3;           
            dr10.Close();
            conn.Close();
        }*/
        /*private void cbMonthChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Graphique nombre incidents mois
            chartNbIncidentMonth.Series["Series1"].XValueType = ChartValueType.Auto;

            String date_chart = ((ComboBoxItem)cbMonthChart.SelectedItem).HiddenValue.ToString() + ((ComboBoxItem)cbYearChart.SelectedItem).HiddenValue.ToString();
            //String dateY_chart = ((ComboBoxItem)cbYearChart.SelectedItem).HiddenValue.ToString() + ((ComboBoxItem)cbYearChart.SelectedItem).HiddenValue.ToString();
            String chartNbMoisSQL = "SELECT CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) as jour, COUNT(*) as nb_incident FROM incidents WHERE CONVERT(varchar(50),MONTH(date_demande))+CONVERT(varchar(50),YEAR(date_demande)) = " + date_chart + " GROUP BY CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) ";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand chartNbMoisComm = new SqlCommand(chartNbMoisSQL, conn);
            SqlDataReader dr11 = chartNbMoisComm.ExecuteReader();
            if (dr11.HasRows)
            {
                while (dr11.Read())
                {
                    chartNbIncidentMonth.Series["Series1"].Points.AddXY(dr11["jour"], dr11["nb_incident"]);
                }
            }
            chartNbIncidentMonth.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chartNbIncidentMonth.Series["Series1"].BorderWidth = 3;
            dr11.Close();
            conn.Close();
        }*/
        private void btnValiderStatsGraph_Click(object sender, EventArgs e)
        {
            // Graphique nombre incidents mois
            chartNbIncidentMonth.Series["Series1"].XValueType = ChartValueType.Auto;
            chartNbIncidentMonth.Series["Series1"].Points.Clear();

            String date_chart = ((ComboBoxItem)cbMonthChart.SelectedItem).HiddenValue.ToString() + ((ComboBoxItem)cbYearChart.SelectedItem).HiddenValue.ToString();
            String chartNbMoisSQL = "SELECT CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) as jour, COUNT(*) as nb_incident FROM incidents WHERE CONVERT(varchar(50),MONTH(date_demande))+CONVERT(varchar(50),YEAR(date_demande)) = " + date_chart + " GROUP BY CONVERT(varchar(50),DAY(date_demande))+'/'+CONVERT(varchar(50),MONTH(date_demande))+'/'+CONVERT(varchar(50),YEAR(date_demande)) ";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            SqlCommand chartNbMoisComm = new SqlCommand(chartNbMoisSQL, conn);
            SqlDataReader dr10 = chartNbMoisComm.ExecuteReader();
            if (dr10.HasRows)
            {
                while (dr10.Read())
                {
                    chartNbIncidentMonth.Series["Series1"].Points.AddXY(dr10["jour"], dr10["nb_incident"]);
                }
            }
            else
            {
                MessageBox.Show("Pas de données pour ce mois");               
            }
            chartNbIncidentMonth.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            chartNbIncidentMonth.Series["Series1"].BorderWidth = 3;
            dr10.Close();
            conn.Close();
        }   

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // RESSOURCES
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Initialisation et design des DataGridView
        private void InitializeDataGridView()
        {
            // Initialize basic DataGridView properties.           
            dgvEquipNonAffecte.BackgroundColor = System.Drawing.Color.LightGray;
            dgvEquipNonAffecte.BorderStyle = BorderStyle.Fixed3D;
            dgvIncidents.BackgroundColor = System.Drawing.Color.LightGray;
            dgvIncidents.BorderStyle = BorderStyle.Fixed3D;
            dgvEquipAffecte.BackgroundColor = System.Drawing.Color.LightGray;
            dgvEquipAffecte.BorderStyle = BorderStyle.Fixed3D;
            dgvSuiviEquipementsV.BackgroundColor = System.Drawing.Color.LightGray;
            dgvSuiviEquipementsV.BorderStyle = BorderStyle.Fixed3D;

            // Set property values appropriate for read-only display and 
            // limited interactivity. 
            dgvEquipNonAffecte.AllowUserToAddRows = false;
            dgvEquipNonAffecte.AllowUserToDeleteRows = false;
            dgvEquipNonAffecte.AllowUserToOrderColumns = true;
            dgvEquipNonAffecte.ReadOnly = true;
            dgvEquipNonAffecte.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvEquipNonAffecte.MultiSelect = false;

            dgvIncidents.AllowUserToAddRows = false;
            dgvIncidents.AllowUserToDeleteRows = false;
            dgvIncidents.AllowUserToOrderColumns = true;
            dgvIncidents.ReadOnly = true;
            dgvIncidents.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvIncidents.MultiSelect = false;

            dgvEquipAffecte.AllowUserToAddRows = false;
            dgvEquipAffecte.AllowUserToDeleteRows = false;
            dgvEquipAffecte.AllowUserToOrderColumns = true;
            dgvEquipAffecte.ReadOnly = true;
            dgvEquipAffecte.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvEquipAffecte.MultiSelect = false;

            dgvSuiviEquipementsV.AllowUserToAddRows = false;
            dgvSuiviEquipementsV.AllowUserToDeleteRows = false;
            dgvSuiviEquipementsV.AllowUserToOrderColumns = true;
            dgvSuiviEquipementsV.ReadOnly = true;
            dgvSuiviEquipementsV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvSuiviEquipementsV.MultiSelect = false;

            // Set the selection background color for all the cells.
            dgvEquipNonAffecte.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvEquipNonAffecte.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            dgvIncidents.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvIncidents.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            dgvEquipAffecte.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvEquipAffecte.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
            dgvSuiviEquipementsV.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            dgvSuiviEquipementsV.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;

            // Set RowHeadersDefaultCellStyle.SelectionBackColor so that its default
            // value won't override DataGridView.DefaultCellStyle.SelectionBackColor.
            dgvEquipNonAffecte.RowHeadersDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Empty;
            dgvIncidents.RowHeadersDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Empty;
            dgvEquipAffecte.RowHeadersDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Empty;
            dgvSuiviEquipementsV.RowHeadersDefaultCellStyle.SelectionBackColor = System.Drawing.Color.Empty;

            // Set the background color for all rows and for alternating rows. 
            // The value for alternating rows overrides the value for all rows. 
            dgvEquipNonAffecte.RowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
            dgvEquipNonAffecte.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dgvIncidents.RowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
            dgvIncidents.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dgvEquipAffecte.RowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
            dgvEquipAffecte.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dgvSuiviEquipementsV.RowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
            dgvSuiviEquipementsV.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.SystemColors.GradientActiveCaption;

            // Set the row and column header styles.
            dgvEquipNonAffecte.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.White;
            dgvEquipNonAffecte.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvEquipNonAffecte.RowHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvIncidents.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.White;
            dgvIncidents.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvIncidents.RowHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvEquipAffecte.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.White;
            dgvEquipAffecte.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvEquipAffecte.RowHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvSuiviEquipementsV.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.White;
            dgvSuiviEquipementsV.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
            dgvSuiviEquipementsV.RowHeadersDefaultCellStyle.BackColor = System.Drawing.Color.Black;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Initialisation et design des ListView
        private void InitializeListView()
        {
            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "Composant";
            columnHeader1.TextAlign = HorizontalAlignment.Left;
            columnHeader1.Width = 100;

            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Valeur";
            columnHeader2.TextAlign = HorizontalAlignment.Left;
            columnHeader2.Width = 100;

            ColumnHeader columnHeader3 = new ColumnHeader();
            columnHeader3.Text = "Unité";
            columnHeader3.TextAlign = HorizontalAlignment.Left;
            columnHeader3.Width = 100;

            ColumnHeader columnHeader4 = new ColumnHeader();
            columnHeader4.Text = "";
            columnHeader4.TextAlign = HorizontalAlignment.Left;
            columnHeader4.Width = 0;

            ColumnHeader columnHeader5 = new ColumnHeader();
            columnHeader5.Text = "";
            columnHeader5.TextAlign = HorizontalAlignment.Left;
            columnHeader5.Width = 0;

            this.lvCarac.Columns.Add(columnHeader1);
            this.lvCarac.Columns.Add(columnHeader2);
            this.lvCarac.Columns.Add(columnHeader3);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Bouton de Déconnexion
        private void button4_Click(object sender, EventArgs e)
        {
            Connexion formConnect = new Connexion();
            formConnect.Show();
            this.Hide();
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Lien A propos
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Aboutcs about = new Aboutcs();
            about.Show();
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // A la fermeture de la form
        private void GestionParc_FormClosed(object sender, FormClosedEventArgs e)
        {
            Connexion formConnect = new Connexion();
            formConnect.Show();
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Tableau de bord Visiteur - Clic boutons de raccourci
        private void button5_Click_1(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = incidentsV;
        }
        private void button7_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = equipementV;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Récupération des données et construction des DataGridView
        // DataGridView dsIncidents : Liste des équipements non affectés
        public void GetData(string selectCommand, string connectionString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter dataadapter = new SqlDataAdapter(selectCommand, connection);
                dsEquipNonAffectes.Clear();
                connection.Open();
                dataadapter.Fill(dsEquipNonAffectes, "equipNonAffectes");
                connection.Close();
                dgvEquipNonAffecte.DataSource = dsEquipNonAffectes.Tables[0];
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }
        // DataGridView dsIncidents : Liste des équipements affectés
        public void GetData2(string selectCommand, string connectionString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter dataadapter = new SqlDataAdapter(selectCommand, connection);
                dsEquipAffectes.Clear();
                connection.Open();
                dataadapter.Fill(dsEquipAffectes, "equipAffectes");
                connection.Close();
                dgvEquipAffecte.DataSource = dsEquipAffectes.Tables[0];
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }
        // DataGridView dsIncidents : Liste des incidents
        private void GetData3(string selectCommand, string connectionString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter dataadapter = new SqlDataAdapter(selectCommand, connection);
                dsIncidents.Clear();
                connection.Open();
                dataadapter.Fill(dsIncidents, "incidents");
                connection.Close();
                dgvIncidents.DataSource = dsIncidents.Tables[0];
                if (dgvIncidents != null)
                {
                    if (role_utilisateur == "2")
                    {
                        dgvIncidents.Columns[6].Visible = false;
                        dgvIncidents.Columns[7].Visible = false;
                        dgvIncidents.Columns[8].Visible = false;
                    }
                }
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }
        // DataGridView dsSuiviEquipementsV : Suivi des équipements du visiteur connecté
        private void GetData4(string selectCommand, string connectionString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter dataadapter = new SqlDataAdapter(selectCommand, connection);
                dsSuiviEquipementsV.Clear();
                connection.Open();
                dataadapter.Fill(dsSuiviEquipementsV, "equipVisiteurs");
                connection.Close();
                dgvSuiviEquipementsV.DataSource = dsSuiviEquipementsV.Tables[0];
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }
        // DataGridView dsSuiviIncidentsV : Suivi des incidents du visiteur connecté
        private void GetData5(string selectCommand, string connectionString)
        {
            try
            {
                SqlConnection connection = new SqlConnection(connectionString);
                SqlDataAdapter dataadapter = new SqlDataAdapter(selectCommand, connection);
                dsSuiviIncidentsV.Clear();
                connection.Open();
                dataadapter.Fill(dsSuiviIncidentsV, "incidentsVisiteurs");
                connection.Close();
                dgvSuiviIncidentsV.DataSource = dsSuiviIncidentsV.Tables[0];
            }
            catch (SqlException)
            {
                MessageBox.Show("To run this example, replace the value of the " +
                    "connectionString variable with a connection string that is " +
                    "valid for your system.");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GetData(listeEquipementSQL, connectionString);
            GetData2(listeEquipementAffectesSQL, connectionString);
        }

        public void reloadData()
        {
            MessageBox.Show("test");
            GetData(listeEquipementSQL, connectionString);
            GetData2(listeEquipementAffectesSQL, connectionString);
        }

        private void reloadData2(object sender, EventArgs e)
        {
            MessageBox.Show("Le dataGridView a été réinitialisé");
            GetData(listeEquipementSQL, connectionString);
            GetData2(listeEquipementAffectesSQL, connectionString);
        }

    }

}
