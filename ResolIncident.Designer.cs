﻿namespace TestGSB1
{
    partial class ResolIncident
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ResolIncident));
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.dtpDateInterv = new System.Windows.Forms.DateTimePicker();
            this.tbSolution = new System.Windows.Forms.TextBox();
            this.cbTechIncident = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbMailVisiteur = new System.Windows.Forms.CheckBox();
            this.btnResolu = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(288, 123);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(76, 22);
            this.numericUpDown2.TabIndex = 19;
            this.numericUpDown2.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(174, 123);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(71, 22);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.Visible = false;
            // 
            // dtpDateInterv
            // 
            this.dtpDateInterv.Location = new System.Drawing.Point(174, 81);
            this.dtpDateInterv.Name = "dtpDateInterv";
            this.dtpDateInterv.Size = new System.Drawing.Size(236, 22);
            this.dtpDateInterv.TabIndex = 16;
            // 
            // tbSolution
            // 
            this.tbSolution.Location = new System.Drawing.Point(30, 208);
            this.tbSolution.Multiline = true;
            this.tbSolution.Name = "tbSolution";
            this.tbSolution.Size = new System.Drawing.Size(380, 147);
            this.tbSolution.TabIndex = 15;
            // 
            // cbTechIncident
            // 
            this.cbTechIncident.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTechIncident.FormattingEnabled = true;
            this.cbTechIncident.Location = new System.Drawing.Point(174, 39);
            this.cbTechIncident.Name = "cbTechIncident";
            this.cbTechIncident.Size = new System.Drawing.Size(236, 24);
            this.cbTechIncident.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Technicien :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Date intervention :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Solution :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Durée d\'intervention :";
            this.label2.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbMailVisiteur);
            this.groupBox2.Controls.Add(this.btnResolu);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.numericUpDown2);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.dtpDateInterv);
            this.groupBox2.Controls.Add(this.tbSolution);
            this.groupBox2.Controls.Add(this.cbTechIncident);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(28, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(435, 499);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Résolution de l\'incident";
            // 
            // cbMailVisiteur
            // 
            this.cbMailVisiteur.AutoSize = true;
            this.cbMailVisiteur.Location = new System.Drawing.Point(63, 377);
            this.cbMailVisiteur.Name = "cbMailVisiteur";
            this.cbMailVisiteur.Size = new System.Drawing.Size(301, 21);
            this.cbMailVisiteur.TabIndex = 25;
            this.cbMailVisiteur.Text = "Envoyer un mail de confirmation au visiteur";
            this.cbMailVisiteur.UseVisualStyleBackColor = true;
            // 
            // btnResolu
            // 
            this.btnResolu.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnResolu.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.btnResolu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnResolu.Location = new System.Drawing.Point(165, 423);
            this.btnResolu.Name = "btnResolu";
            this.btnResolu.Size = new System.Drawing.Size(94, 48);
            this.btnResolu.TabIndex = 24;
            this.btnResolu.Text = "Résolu";
            this.btnResolu.UseVisualStyleBackColor = false;
            this.btnResolu.Click += new System.EventHandler(this.btnResolu_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(371, 128);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 17);
            this.label11.TabIndex = 20;
            this.label11.Text = "min";
            this.label11.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(252, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(16, 17);
            this.label10.TabIndex = 18;
            this.label10.Text = "h";
            this.label10.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(-31, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(574, 45);
            this.panel1.TabIndex = 38;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(162, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(211, 28);
            this.label12.TabIndex = 19;
            this.label12.Text = "Résoudre l\'incident";
            // 
            // ResolIncident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(497, 598);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ResolIncident";
            this.Text = "Résolution d\'incidents";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ResolIncident_FormClosed);
            this.Load += new System.EventHandler(this.ResolIncident_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.DateTimePicker dtpDateInterv;
        private System.Windows.Forms.TextBox tbSolution;
        private System.Windows.Forms.ComboBox cbTechIncident;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox cbMailVisiteur;
        private System.Windows.Forms.Button btnResolu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label12;
    }
}