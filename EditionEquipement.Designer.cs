﻿namespace TestGSB1
{
    partial class EditionEquipement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditionEquipement));
            this.label1 = new System.Windows.Forms.Label();
            this.LabIdEquipement = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDateAchat = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.nupGarantie = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cbVisiteur = new System.Windows.Forms.ComboBox();
            this.cbEtat = new System.Windows.Forms.ComboBox();
            this.cbFournisseur = new System.Windows.Forms.ComboBox();
            this.cbTypeEquip = new System.Windows.Forms.ComboBox();
            this.cbMarque = new System.Windows.Forms.ComboBox();
            this.dtpDateAssignation = new System.Windows.Forms.DateTimePicker();
            this.btnSaveEquip = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbValeurComposant = new System.Windows.Forms.TextBox();
            this.cbUniteComposant = new System.Windows.Forms.ComboBox();
            this.cbTypeComposant = new System.Windows.Forms.ComboBox();
            this.lvCarac = new System.Windows.Forms.ListView();
            this.btnAddCarac = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.clbLogiciels = new System.Windows.Forms.CheckedListBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDeleteEquipement = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.nupGarantie)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID :";
            // 
            // LabIdEquipement
            // 
            this.LabIdEquipement.AutoSize = true;
            this.LabIdEquipement.Location = new System.Drawing.Point(205, 79);
            this.LabIdEquipement.Name = "LabIdEquipement";
            this.LabIdEquipement.Size = new System.Drawing.Size(46, 17);
            this.LabIdEquipement.TabIndex = 1;
            this.LabIdEquipement.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Date d\'achat :";
            // 
            // dtpDateAchat
            // 
            this.dtpDateAchat.Location = new System.Drawing.Point(208, 117);
            this.dtpDateAchat.Name = "dtpDateAchat";
            this.dtpDateAchat.Size = new System.Drawing.Size(200, 22);
            this.dtpDateAchat.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 167);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Garantie :";
            // 
            // nupGarantie
            // 
            this.nupGarantie.Location = new System.Drawing.Point(208, 165);
            this.nupGarantie.Name = "nupGarantie";
            this.nupGarantie.Size = new System.Drawing.Size(200, 22);
            this.nupGarantie.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 299);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Assigné à :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 254);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Etat :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(63, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Date d\'assignation :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(63, 432);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "Marque :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(63, 387);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "Type :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(63, 344);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "Fournisseur :";
            // 
            // cbVisiteur
            // 
            this.cbVisiteur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVisiteur.FormattingEnabled = true;
            this.cbVisiteur.Location = new System.Drawing.Point(208, 296);
            this.cbVisiteur.Name = "cbVisiteur";
            this.cbVisiteur.Size = new System.Drawing.Size(200, 24);
            this.cbVisiteur.TabIndex = 12;
            // 
            // cbEtat
            // 
            this.cbEtat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEtat.FormattingEnabled = true;
            this.cbEtat.Location = new System.Drawing.Point(208, 251);
            this.cbEtat.Name = "cbEtat";
            this.cbEtat.Size = new System.Drawing.Size(200, 24);
            this.cbEtat.TabIndex = 13;
            // 
            // cbFournisseur
            // 
            this.cbFournisseur.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFournisseur.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbFournisseur.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFournisseur.FormattingEnabled = true;
            this.cbFournisseur.Location = new System.Drawing.Point(208, 341);
            this.cbFournisseur.Name = "cbFournisseur";
            this.cbFournisseur.Size = new System.Drawing.Size(200, 24);
            this.cbFournisseur.TabIndex = 14;
            // 
            // cbTypeEquip
            // 
            this.cbTypeEquip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeEquip.FormattingEnabled = true;
            this.cbTypeEquip.Location = new System.Drawing.Point(208, 384);
            this.cbTypeEquip.Name = "cbTypeEquip";
            this.cbTypeEquip.Size = new System.Drawing.Size(200, 24);
            this.cbTypeEquip.TabIndex = 15;
            // 
            // cbMarque
            // 
            this.cbMarque.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbMarque.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbMarque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarque.FormattingEnabled = true;
            this.cbMarque.Location = new System.Drawing.Point(208, 429);
            this.cbMarque.Name = "cbMarque";
            this.cbMarque.Size = new System.Drawing.Size(200, 24);
            this.cbMarque.TabIndex = 16;
            // 
            // dtpDateAssignation
            // 
            this.dtpDateAssignation.Location = new System.Drawing.Point(208, 206);
            this.dtpDateAssignation.Name = "dtpDateAssignation";
            this.dtpDateAssignation.Size = new System.Drawing.Size(200, 22);
            this.dtpDateAssignation.TabIndex = 17;
            // 
            // btnSaveEquip
            // 
            this.btnSaveEquip.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSaveEquip.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveEquip.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSaveEquip.Location = new System.Drawing.Point(672, 484);
            this.btnSaveEquip.Name = "btnSaveEquip";
            this.btnSaveEquip.Size = new System.Drawing.Size(127, 42);
            this.btnSaveEquip.TabIndex = 18;
            this.btnSaveEquip.Text = "Enregistrer";
            this.btnSaveEquip.UseVisualStyleBackColor = false;
            this.btnSaveEquip.Click += new System.EventHandler(this.btnSaveEquip_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(315, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(256, 28);
            this.label10.TabIndex = 19;
            this.label10.Text = "Edition de l\'équipement";
            // 
            // tbValeurComposant
            // 
            this.tbValeurComposant.AccessibleDescription = "";
            this.tbValeurComposant.Location = new System.Drawing.Point(615, 117);
            this.tbValeurComposant.Multiline = true;
            this.tbValeurComposant.Name = "tbValeurComposant";
            this.tbValeurComposant.Size = new System.Drawing.Size(69, 24);
            this.tbValeurComposant.TabIndex = 30;
            this.tbValeurComposant.Text = "Valeur";
            // 
            // cbUniteComposant
            // 
            this.cbUniteComposant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUniteComposant.FormattingEnabled = true;
            this.cbUniteComposant.Location = new System.Drawing.Point(690, 117);
            this.cbUniteComposant.Name = "cbUniteComposant";
            this.cbUniteComposant.Size = new System.Drawing.Size(63, 24);
            this.cbUniteComposant.TabIndex = 29;
            // 
            // cbTypeComposant
            // 
            this.cbTypeComposant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeComposant.FormattingEnabled = true;
            this.cbTypeComposant.Location = new System.Drawing.Point(449, 117);
            this.cbTypeComposant.Name = "cbTypeComposant";
            this.cbTypeComposant.Size = new System.Drawing.Size(160, 24);
            this.cbTypeComposant.TabIndex = 28;
            // 
            // lvCarac
            // 
            this.lvCarac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvCarac.FullRowSelect = true;
            this.lvCarac.GridLines = true;
            this.lvCarac.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvCarac.Location = new System.Drawing.Point(449, 151);
            this.lvCarac.Name = "lvCarac";
            this.lvCarac.Size = new System.Drawing.Size(339, 124);
            this.lvCarac.TabIndex = 27;
            this.lvCarac.UseCompatibleStateImageBehavior = false;
            this.lvCarac.View = System.Windows.Forms.View.Details;
            // 
            // btnAddCarac
            // 
            this.btnAddCarac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddCarac.BackgroundImage")));
            this.btnAddCarac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddCarac.Location = new System.Drawing.Point(759, 114);
            this.btnAddCarac.Name = "btnAddCarac";
            this.btnAddCarac.Size = new System.Drawing.Size(29, 28);
            this.btnAddCarac.TabIndex = 26;
            this.btnAddCarac.UseVisualStyleBackColor = true;
            this.btnAddCarac.Click += new System.EventHandler(this.btnAddCarac_Click);
            // 
            // label12
            // 
            this.label12.AllowDrop = true;
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(446, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(191, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "Caractéristiques techniques :";
            // 
            // clbLogiciels
            // 
            this.clbLogiciels.CheckOnClick = true;
            this.clbLogiciels.FormattingEnabled = true;
            this.clbLogiciels.Location = new System.Drawing.Point(449, 330);
            this.clbLogiciels.Name = "clbLogiciels";
            this.clbLogiciels.Size = new System.Drawing.Size(339, 123);
            this.clbLogiciels.TabIndex = 33;
            // 
            // label11
            // 
            this.label11.AllowDrop = true;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(446, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 17);
            this.label11.TabIndex = 34;
            this.label11.Text = "Logiciels :";
            // 
            // btnDeleteEquipement
            // 
            this.btnDeleteEquipement.BackColor = System.Drawing.Color.Red;
            this.btnDeleteEquipement.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.btnDeleteEquipement.ForeColor = System.Drawing.SystemColors.Control;
            this.btnDeleteEquipement.Location = new System.Drawing.Point(55, 484);
            this.btnDeleteEquipement.Name = "btnDeleteEquipement";
            this.btnDeleteEquipement.Size = new System.Drawing.Size(115, 42);
            this.btnDeleteEquipement.TabIndex = 35;
            this.btnDeleteEquipement.Text = "Supprimer";
            this.btnDeleteEquipement.UseVisualStyleBackColor = false;
            this.btnDeleteEquipement.Click += new System.EventHandler(this.btnDeleteEquipement_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-16, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(892, 45);
            this.panel1.TabIndex = 36;
            // 
            // EditionEquipement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(854, 538);
            this.Controls.Add(this.btnDeleteEquipement);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.clbLogiciels);
            this.Controls.Add(this.tbValeurComposant);
            this.Controls.Add(this.cbUniteComposant);
            this.Controls.Add(this.cbTypeComposant);
            this.Controls.Add(this.lvCarac);
            this.Controls.Add(this.btnAddCarac);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.btnSaveEquip);
            this.Controls.Add(this.dtpDateAssignation);
            this.Controls.Add(this.cbMarque);
            this.Controls.Add(this.cbTypeEquip);
            this.Controls.Add(this.cbFournisseur);
            this.Controls.Add(this.cbEtat);
            this.Controls.Add(this.cbVisiteur);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nupGarantie);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpDateAchat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LabIdEquipement);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditionEquipement";
            this.Text = "Editer l\'équipement";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditionEquipement_FormClosed);
            this.Load += new System.EventHandler(this.EditionEquipement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupGarantie)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabIdEquipement;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpDateAchat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nupGarantie;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbVisiteur;
        private System.Windows.Forms.ComboBox cbEtat;
        private System.Windows.Forms.ComboBox cbFournisseur;
        private System.Windows.Forms.ComboBox cbTypeEquip;
        private System.Windows.Forms.ComboBox cbMarque;
        private System.Windows.Forms.DateTimePicker dtpDateAssignation;
        private System.Windows.Forms.Button btnSaveEquip;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbValeurComposant;
        private System.Windows.Forms.ComboBox cbUniteComposant;
        private System.Windows.Forms.ComboBox cbTypeComposant;
        private System.Windows.Forms.ListView lvCarac;
        private System.Windows.Forms.Button btnAddCarac;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckedListBox clbLogiciels;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnDeleteEquipement;
        private System.Windows.Forms.Panel panel1;
    }
}