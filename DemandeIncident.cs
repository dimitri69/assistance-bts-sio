﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace TestGSB1
{
    public partial class DemandeIncident : Form
    {
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private String idUser;

        public DemandeIncident(String id_utilisateur)
        {
            InitializeComponent();
            idUser = id_utilisateur;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String id_demandeur = ((ComboBoxItem)cbDmandeurIncident.SelectedItem).HiddenValue;
            cbEquipementIncident.Items.Clear();
            // Remplissage de la Combobox comboBox2 : Equipements
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String etats_equipSQL = "SELECT eq.id as id, CONVERT(varchar(50),eq.id) + ' - ' + ty.libelle + ' ' + ma.nom +' (' + ut.prenom+' '+ut.nom + ')' as 'Equipement' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs = " + id_demandeur;
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    cbEquipementIncident.Items.Add(new ComboBoxItem(dr2["Equipement"].ToString(), dr2["id"].ToString()));
                }
            }
            dr2.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            
            String objet = tbObjet.Text.ToString();
            String id_demandeur = ((ComboBoxItem)cbDmandeurIncident.SelectedItem).HiddenValue;
            String id_equipement = ((ComboBoxItem)cbEquipementIncident.SelectedItem).HiddenValue;
            String id_niveaux = tbNiveauIncident.Value.ToString();         

            String upd_affectSQL = "INSERT INTO incidents (date_demande, objet, date_intervention, solution, duree_intevrention, date_modification, id_utilisateurs, id_equipements, id_statuts, id_niveaux, id_utilisateurs_1) VALUES(@date_demande, @objet, NULL, NULL, NULL, CONVERT(VARCHAR(19),GETDATE(),120), @id_demandeur, @id_equipement, '1', @id_niveaux, NULL)";
            SqlCommand upd_affectComm = new SqlCommand(upd_affectSQL, conn);

            upd_affectComm.Parameters.AddWithValue("@date_demande", dateTimePicker1.Value);
            upd_affectComm.Parameters.AddWithValue("@objet", objet);
            upd_affectComm.Parameters.AddWithValue("@id_demandeur", id_demandeur);
            upd_affectComm.Parameters.AddWithValue("@id_equipement", id_equipement);
            upd_affectComm.Parameters.AddWithValue("@id_niveaux", id_niveaux);

            conn.Open();
            int rowsAffected = upd_affectComm.ExecuteNonQuery();
            this.Hide();
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();


        }

        private void DemandeIncident_Load(object sender, EventArgs e)
        {                    
            // Connection à la BDD
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            // Remplissage de la Combobox comboBox1 : Demandeur
            String types_equipSQL = "SELECT id, nom+' '+prenom as nom_prenom FROM utilisateurs";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbDmandeurIncident.Items.Add(new ComboBoxItem(dr["nom_prenom"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();

            // Remplissage de la Combobox comboBox2 : Equipements
            String etats_equipSQL = "SELECT eq.id as id, CONVERT(varchar(50),eq.id) + ' - ' + ty.libelle + ' ' + ma.nom +' (' + ut.prenom+' '+ut.nom + ')' as 'Equipement' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is not null";
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    cbEquipementIncident.Items.Add(new ComboBoxItem(dr2["Equipement"].ToString(), dr2["id"].ToString()));
                }
            }
            dr2.Close();
          
        }

        private void trackBar1_MouseHover(object sender, EventArgs e)
        {
            // Ttexte d'aide caché
            ToolTip toolTip1 = new ToolTip();           
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 1000;
            toolTip1.ReshowDelay = 500;            
            toolTip1.SetToolTip(this.tbNiveauIncident, "Degré de complexité de la demande \n1.	Demande basique (niveau par défaut), ne nécessite pas une intervention logicielle ou matérielle, dépannage en direct avec l'interlocuteur. Exemple : perte d'identifiant de messagerie, déblocage d'un rapport verrouillé, etc \n2.	Demande nécessitant une intervention à distance. \n3.	Demande nécessitant une intervention matérielle ou une réinstallation logicielle lourde \n4.	Demande nécessitant l'adaptation du site Web de gestion des Comptes-rendus et des frais de visite");  
        }

        private void DemandeIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();
        }
    }
}
