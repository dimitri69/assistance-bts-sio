﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGSB1
{
    public partial class EditionEquipement : Form
    {
        private String IdEquipement;
        private String idUser;
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private String listeEquipementSQL = "SELECT eq.id as 'ID', ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie (an)', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is null";
        private String listeEquipementAffectesSQL = "SELECT eq.id, ty.libelle as 'Type', ma.nom as 'Marque', eq.date_achat as 'Date achat', eq.garantie as 'Garantie', eq.date_assignation as 'Assigné le', ut.prenom+' '+ut.nom as 'Assigné à', eq.date_modification as 'Modifié le' FROM equipements as eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques WHERE eq.id_utilisateurs is not null";
        
        private bool caracAjoute;

        public EditionEquipement(String IdEquipement_ToEdit, String id_utilisateur)
        {
            InitializeComponent();
            InitializeListView();
            IdEquipement = IdEquipement_ToEdit;
            idUser = id_utilisateur;
            caracAjoute = false;            
        }

        private void EditionEquipement_Load(object sender, EventArgs e)
        {            
            // Connection à la BDD
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            // Remplissage de la Combobox comboBox1 : Type de l'équipement
            String types_equipSQL = "SELECT id as id, libelle as libelle FROM types";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbTypeEquip.Items.Add(new ComboBoxItem(dr["libelle"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();

            // Remplissage de la Combobox comboBox3 : Etat de l'équipement
            String etats_equipSQL = "SELECT id as id, libelle as libelle FROM etats";
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    cbEtat.Items.Add(new ComboBoxItem(dr2["libelle"].ToString(), dr2["id"].ToString()));
                }
            }
            dr2.Close();

            // Remplissage de la Combobox comboBox3 : Marque de l'équipement
            String marques_equipSQL = "SELECT id as id, nom as nom FROM marques";
            SqlCommand marques_equipComm = new SqlCommand(marques_equipSQL, conn);
            SqlDataReader dr3 = marques_equipComm.ExecuteReader();
            if (dr3.HasRows)
            {
                while (dr3.Read())
                {
                    cbMarque.Items.Add(new ComboBoxItem(dr3["nom"].ToString(), dr3["id"].ToString()));
                }
            }
            dr3.Close();

            // Remplissage de la Combobox comboBox4 : Fournisseur de l'équipement
            String fournisseurs_equipSQL = "SELECT id as id, nom as nom FROM fournisseurs";
            SqlCommand fournisseurs_equipComm = new SqlCommand(fournisseurs_equipSQL, conn);
            SqlDataReader dr4 = fournisseurs_equipComm.ExecuteReader();
            if (dr4.HasRows)
            {
                while (dr4.Read())
                {
                    cbFournisseur.Items.Add(new ComboBoxItem(dr4["nom"].ToString(), dr4["id"].ToString()));
                }
            }
            dr4.Close();

            // Remplissage de la Combobox cbTypeComposant : Type de matériel de l'équipement
            String types_composantSQL = "SELECT id as id, libelle as libelle FROM type_materiels";
            SqlCommand types_composantComm = new SqlCommand(types_composantSQL, conn);
            SqlDataReader dr5 = types_composantComm.ExecuteReader();
            if (dr5.HasRows)
            {
                while (dr5.Read())
                {
                    cbTypeComposant.Items.Add(new ComboBoxItem(dr5["libelle"].ToString(), dr5["id"].ToString()));
                }
            }
            dr5.Close();

            // Remplissage de la Combobox cbTypeComposant : Unité de matériel de l'équipement
            String unite_composantSQL = "SELECT id as id, libelle as libelle FROM unites_materiels";
            SqlCommand unite_composantComm = new SqlCommand(unite_composantSQL, conn);
            SqlDataReader dr6 = unite_composantComm.ExecuteReader();
            if (dr6.HasRows)
            {
                while (dr6.Read())
                {
                    cbUniteComposant.Items.Add(new ComboBoxItem(dr6["libelle"].ToString(), dr6["id"].ToString()));
                }
            }
            dr6.Close();


            string assigneaSQL = "select id, prenom+' '+nom as 'visiteur' from utilisateurs";
            SqlCommand assigneaComm = new SqlCommand(assigneaSQL, conn);
            SqlDataReader dr7 = assigneaComm.ExecuteReader();
            if (dr7.HasRows)
            {
                while (dr7.Read())
                {
                    cbVisiteur.Items.Add(new ComboBoxItem(dr7["visiteur"].ToString(), dr7["id"].ToString()));
                }
            }
            dr7.Close();

            string logicielsSQL = "select id, nom+' '+version+' ('+editeur+')' as 'Caracteristique' from logiciels";
            SqlCommand logicielsComm = new SqlCommand(logicielsSQL, conn);
            SqlDataReader dr8 = logicielsComm.ExecuteReader();
            if (dr8.HasRows)
            {
                while (dr8.Read())
                {
                    clbLogiciels.Items.Add(new ComboBoxItem(dr8["Caracteristique"].ToString(), dr8["id"].ToString()));
                }
            }
            dr8.Close();

            var dateToConvert = "Wed Oct 02 2013 00:00:00 GMT+0100 (GMT Daylight Time)";
            var format = "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz '(GMT Daylight Time)'";

            String equipementSQL = "SELECT date_achat as 'date_achat', garantie as 'garantie', date_assignation, date_modification, id_etats, id_utilisateurs, id_fournisseurs, id_types, id_marques, et.libelle as etat, ma.nom as marque, fo.nom as fournisseur, ty.libelle as type_eq, ut.prenom+' '+ut.nom as proprietaire FROM equipements eq LEFT JOIN types ty ON ty.id = eq.id_types LEFT JOIN utilisateurs ut ON ut.id = eq.id_utilisateurs LEFT JOIN fournisseurs fo ON fo.id = eq.id_fournisseurs LEFT JOIN marques ma ON ma.id = eq.id_marques LEFT JOIN etats et ON et.id = eq.id_etats  where eq.id = " + IdEquipement;
            SqlCommand equipementComm = new SqlCommand(equipementSQL, conn);
            SqlDataReader dr9 = equipementComm.ExecuteReader();
            dr9.Read();            

            LabIdEquipement.Text = IdEquipement;
            DateTime dateAchat = Convert.ToDateTime(dr9["date_achat"]);
            dtpDateAchat.Value = dateAchat;
            nupGarantie.Text = dr9["garantie"].ToString();
            DateTime dateAssignation = Convert.ToDateTime(dr9["date_achat"]);
            dtpDateAssignation.Value = dateAssignation;
            cbEtat.Text = dr9["etat"].ToString();
            cbVisiteur.Text = dr9["proprietaire"].ToString();
            cbFournisseur.Text = dr9["fournisseur"].ToString();
            cbTypeEquip.Text = dr9["type_eq"].ToString();
            cbMarque.Text = dr9["marque"].ToString();            
            dr9.Close();

            String logiciels_equipSQL = "SELECT nom+' '+version+' ('+editeur+')' as logiciels_checked FROM logiciel_equipement le LEFT JOIN logiciels lo ON lo.id = le.id_logiciels where le.id_equipements = " + IdEquipement;
            SqlCommand logiciels_equipComm = new SqlCommand(logiciels_equipSQL, conn);
            SqlDataReader dr10 = logiciels_equipComm.ExecuteReader();
            if (dr10.HasRows)
            {
                while (dr10.Read())
                {
                    //clbLogiciels.Items.Add(dr10["logiciels_checked"]);
                    for (int i = 0; i <= (clbLogiciels.Items.Count - 1); i++)
                    {
                        String ItemSelected = dr10["logiciels_checked"].ToString();
                        if (ItemSelected == clbLogiciels.Items[i].ToString())
                        {                                                      
                            clbLogiciels.SetItemChecked(i, true);
                        }
                    }
                   
                }
            }
            dr10.Close();

            String carac_equipSQL = "SELECT tm.libelle as carac_type, CONVERT(VARCHAR(80),valeur) as valeur, um.libelle as unite FROM caracteristique_equipement ce LEFT JOIN caracteristiques ca ON ca.id = ce.id_caracteristiques LEFT JOIN unites_materiels um ON um.id = ca.id_unites_materiels LEFT JOIN type_materiels tm ON tm.id = ca.id_type_materiels WHERE ce.id_equipements = " + IdEquipement;
            SqlCommand carac_equipComm = new SqlCommand(carac_equipSQL, conn);
            SqlDataReader dr11 = carac_equipComm.ExecuteReader();
            if (dr11.HasRows)
            {
                while (dr11.Read())
                {                   
                    string[] row = { dr11["carac_type"].ToString(), dr11["valeur"].ToString(), dr11["unite"].ToString() };                                        
                    ListViewItem itm;
                    itm = new ListViewItem(row);
                    lvCarac.Items.Add(itm);
                }
            }
            dr11.Close();

            conn.Close();

        }

        private void btnDeleteEquipement_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String delSQL = "DELETE FROM equipements WHERE id = @idEquipement";
            SqlCommand delComm = new SqlCommand(delSQL, conn);
            delComm.Parameters.AddWithValue("@idEquipement", IdEquipement.ToString());
            int rowsAffected = delComm.ExecuteNonQuery();
            if (rowsAffected > 0)
            {
                MessageBox.Show("Equipement supprimé.");
            }
            this.Hide();
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();
            gp.GetData(listeEquipementSQL, connectionString);
            gp.GetData2(listeEquipementAffectesSQL, connectionString);
        }

        private void InitializeListView()
        {
            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "Composant";
            columnHeader1.TextAlign = HorizontalAlignment.Left;
            //columnHeader1.Width = 146;

            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Valeur";
            columnHeader2.TextAlign = HorizontalAlignment.Left;
            //columnHeader2.Width = 142;

            ColumnHeader columnHeader3 = new ColumnHeader();
            columnHeader3.Text = "Unité";
            columnHeader3.TextAlign = HorizontalAlignment.Left;
            //columnHeader3.Width = 142;

            ColumnHeader columnHeader4 = new ColumnHeader();
            columnHeader4.Text = "";
            columnHeader4.TextAlign = HorizontalAlignment.Left;
            columnHeader4.Width = 0;

            ColumnHeader columnHeader5 = new ColumnHeader();
            columnHeader5.Text = "";
            columnHeader5.TextAlign = HorizontalAlignment.Left;
            columnHeader5.Width = 0;

            this.lvCarac.Columns.Add(columnHeader1);
            this.lvCarac.Columns.Add(columnHeader2);
            this.lvCarac.Columns.Add(columnHeader3);
        }

        private void btnSaveEquip_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String upd_equipSQL = "UPDATE equipements SET date_achat = @date_achat, garantie = @garantie, date_assignation = @date_assignation, date_modification = CONVERT(VARCHAR(19),GETDATE(),120), id_etats = @id_etats, id_utilisateurs = @id_utilisateurs, id_fournisseurs = @id_fournisseurs, id_types = @id_types, id_marques = @id_marques WHERE id = @id_equipement";
            SqlCommand upd_equipComm = new SqlCommand(upd_equipSQL, conn);
            upd_equipComm.Parameters.AddWithValue("@date_achat", dtpDateAchat.Value);
            upd_equipComm.Parameters.AddWithValue("@garantie", nupGarantie.Value);
            upd_equipComm.Parameters.AddWithValue("@date_assignation", dtpDateAssignation.Value);
            upd_equipComm.Parameters.AddWithValue("@id_etats", ((ComboBoxItem)cbEtat.SelectedItem).HiddenValue).ToString();
            string id_utilisateurs = cbVisiteur.Text.ToString();
            if(id_utilisateurs != ""){
                upd_equipComm.Parameters.AddWithValue("@id_utilisateurs", ((ComboBoxItem)cbVisiteur.SelectedItem).HiddenValue).ToString();
            }
            else
            {
                upd_equipComm.Parameters.AddWithValue("@id_utilisateurs", DBNull.Value);
            }
            upd_equipComm.Parameters.AddWithValue("@id_fournisseurs", ((ComboBoxItem)cbFournisseur.SelectedItem).HiddenValue).ToString();
            upd_equipComm.Parameters.AddWithValue("@id_types", ((ComboBoxItem)cbTypeEquip.SelectedItem).HiddenValue).ToString();
            upd_equipComm.Parameters.AddWithValue("@id_marques", ((ComboBoxItem)cbMarque.SelectedItem).HiddenValue).ToString();
            upd_equipComm.Parameters.AddWithValue("@id_equipement", IdEquipement);            
            upd_equipComm.ExecuteScalar();

            if (caracAjoute)
            {
                // Gestion des Caractéristiques
                String delCaracSQL = "DELETE FROM caracteristique_equipement WHERE id_equipements = " + IdEquipement;
                SqlCommand delCaracComm = new SqlCommand(delCaracSQL, conn);
                delCaracComm.ExecuteScalar();
                for (int i = 0; i < lvCarac.Items.Count; i++)
                {
                    // Ajout de nouvelles caractéristiques dans la table caracteristique                            
                    String valeur_composant = lvCarac.Items[i].SubItems[1].Text;
                    String type_composant = lvCarac.Items[i].SubItems[3].Text;
                    String unite_composant = lvCarac.Items[i].SubItems[4].Text;
                    String ins2SQL = "INSERT INTO caracteristiques (valeur, id_unites_materiels, id_type_materiels) VALUES(@valeur_composant, @type_composant, @unite_composant) SELECT SCOPE_IDENTITY()";
                    SqlCommand ins2Comm = new SqlCommand(ins2SQL, conn);
                    ins2Comm.Parameters.AddWithValue("@valeur_composant", valeur_composant);
                    ins2Comm.Parameters.AddWithValue("@type_composant", type_composant);
                    ins2Comm.Parameters.AddWithValue("@unite_composant", unite_composant);
                    int idCaracteristique = Convert.ToInt32(ins2Comm.ExecuteScalar());

                    // Gestion de la liaison entre la table caracteristiques et la table equipements  
                    if (idCaracteristique > 0)
                    {
                        String ins3SQL = "INSERT INTO caracteristique_equipement (id_equipements, id_caracteristiques) VALUES(@id_equipements, @id_caracteristiques)";
                        SqlCommand ins3Comm = new SqlCommand(ins3SQL, conn);
                        ins3Comm.Parameters.AddWithValue("@id_equipements", IdEquipement.ToString());
                        ins3Comm.Parameters.AddWithValue("@id_caracteristiques", idCaracteristique.ToString());
                        int rowsAffected3 = ins3Comm.ExecuteNonQuery();
                    }
                }
            }

            // Gestion des logiciels de l'équipement
            String delLogicielSQL = "DELETE FROM logiciel_equipement WHERE id_equipements = " + IdEquipement;
            SqlCommand delLogicielComm = new SqlCommand(delLogicielSQL, conn);
            delLogicielComm.ExecuteScalar();
            for (int i = 0; i <= (clbLogiciels.Items.Count - 1); i++)
            {
                if (clbLogiciels.GetItemChecked(i))
                {
                    String ins4SQL = "INSERT INTO logiciel_equipement (id_equipements, id_logiciels) VALUES(@id_equipements, @id_logiciels)";
                    SqlCommand ins4Comm = new SqlCommand(ins4SQL, conn);
                    ins4Comm.Parameters.AddWithValue("@id_equipements", IdEquipement);
                    ins4Comm.Parameters.AddWithValue("@id_logiciels", ((ComboBoxItem)clbLogiciels.Items[i]).HiddenValue.ToString());
                    int rowsAffected4 = ins4Comm.ExecuteNonQuery();                        
                }
            }            
            MessageBox.Show("Equipement mis à jour avec succès", "Mise à jour des données réussi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();
            conn.Close();

        }

        private void EditionEquipement_FormClosed(object sender, FormClosedEventArgs e)
        {            
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();
        }

        private void btnAddCarac_Click(object sender, EventArgs e)
        {
            string[] row = { ((ComboBoxItem)cbTypeComposant.SelectedItem).ToString(), tbValeurComposant.Text.ToString(), ((ComboBoxItem)cbUniteComposant.SelectedItem).ToString(), ((ComboBoxItem)cbTypeComposant.SelectedItem).HiddenValue, ((ComboBoxItem)cbUniteComposant.SelectedItem).HiddenValue };
            ListViewItem itm;
            itm = new ListViewItem(row);
            lvCarac.Items.Add(itm);
            caracAjoute = true;
        }
    }
}
