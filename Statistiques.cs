﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGSB1
{
    class Statistiques
    {
        private String idUser;
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public Statistiques(String idUserSession)
        {
            idUser = idUserSession;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// UTILISATEURS

        public String getNbVisiteurs(){
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_visiteurs FROM utilisateurs where id_profils = 1";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_visiteurs"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbTechniciens()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_techniciens FROM utilisateurs where id_profils = 2";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_techniciens"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbUtilisateurs()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_utilisateurs FROM utilisateurs";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_utilisateurs"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// EQUIPEMENTS

        public String getNbEquipements()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_equipements FROM equipements";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_equipements"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbMesEquipements(String id_utilisateur)
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_equipements FROM equipements WHERE id_utilisateurs = " + id_utilisateur;
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_equipements"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbEquipementsAffectes()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_equipements FROM equipements WHERE id_utilisateurs IS NOT NULL";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_equipements"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbEquipementsNonAffectes()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_equipements FROM equipements WHERE id_utilisateurs IS NULL";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_equipements"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbEquipementsOfMonth()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_equipements FROM equipements WHERE MONTH(date_assignation) = MONTH(CONVERT(VARCHAR(19),GETDATE(),120)) AND id_utilisateurs IS NOT NULL";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_equipements"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////
        //////// INCIDENTS
        public String getTmpsMoyDemande()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT (SUM(Duration) / SUM(Nb_Incidents)) AS tempsMoyenIncident FROM ( SELECT 0 as Nb_Incidents, SUM(DATEDIFF(day,date_intervention,date_demande)) AS Duration FROM incidents WHERE DATEDIFF(day,date_demande,date_intervention) IS NOT NULL UNION SELECT COUNT(*) AS Nb_Incidents, 0 as Duration FROM incidents WHERE DATEDIFF(day,date_demande,date_intervention) IS NOT NULL) as r";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["tempsMoyenIncident"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbIncidents()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbIncidentsResolus()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents WHERE id_statuts = 2";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbMesIncidents(String id_utilisateur)
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents WHERE id_utilisateurs = " + id_utilisateur;
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbMesIncidentsResolus(String id_utilisateur)
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents WHERE id_statuts = 2 AND id_utilisateurs = " + id_utilisateur;
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbMesIncidentsEnAttente(String id_utilisateur)
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents WHERE id_statuts = 1 AND id_utilisateurs = " + id_utilisateur;
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }

        public String getNbIncidentsOfMonth()
        {
            String nbVisiteurs = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String equipSQL = "SELECT COUNT(*) as nb_incidents FROM incidents WHERE date_intervention IS NOT NULL AND MONTH(date_intervention) = MONTH(CONVERT(VARCHAR(19),GETDATE(),120))";
            SqlCommand equipComm = new SqlCommand(equipSQL, conn);
            SqlDataReader dr = equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    nbVisiteurs = dr["nb_incidents"].ToString();
                }
            }
            dr.Close();
            return nbVisiteurs;
        }


    }
}
