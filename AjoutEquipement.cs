﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;

namespace TestGSB1
{
    public partial class AjoutEquipement : Form
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///     VARIABLES PRIVEES ET DECLARATION
        ///     
        private String idUser;
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public AjoutEquipement(string id_utilisateur)
        {
            InitializeComponent();
            InitializeListView();
            idUser = id_utilisateur;               
        }

        /*
         *  Button button1 : Ajout d'un nouvel équipement
         */
        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            bool errorSQL = false;

            // Ajout de la nouvelle entrée dans la table équipements        
            String garantie = numericUpDown1.Value.ToString();
            String id_etats = ((ComboBoxItem)comboBox3.SelectedItem).HiddenValue;
            String id_fournisseurs = ((ComboBoxItem)comboBox4.SelectedItem).HiddenValue;
            String id_types = ((ComboBoxItem)cbTypeEquip.SelectedItem).HiddenValue; ;
            String id_marques = ((ComboBoxItem)cbMarqueEquip.SelectedItem).HiddenValue;
            String ins1SQL = "INSERT INTO equipements (date_achat, garantie, date_assignation, date_modification, id_etats, id_utilisateurs, id_fournisseurs, id_types, id_marques) VALUES(@date_achat, @garantie, NULL, CONVERT(VARCHAR(19),GETDATE(),120), @id_etats, NULL, @id_fournisseurs, @id_types, @id_marques) SELECT SCOPE_IDENTITY()";
            SqlCommand ins1Comm = new SqlCommand(ins1SQL, conn);
            ins1Comm.Parameters.AddWithValue("@date_achat", dateTimePicker1.Value);
            ins1Comm.Parameters.AddWithValue("@garantie", garantie);
            ins1Comm.Parameters.AddWithValue("@date_assignation", dateTimePicker1.Value);
            ins1Comm.Parameters.AddWithValue("@date_modification", dateTimePicker1.Value);
            ins1Comm.Parameters.AddWithValue("@id_etats", id_etats);
            ins1Comm.Parameters.AddWithValue("@id_fournisseurs", id_fournisseurs);
            ins1Comm.Parameters.AddWithValue("@id_types", id_types);
            ins1Comm.Parameters.AddWithValue("@id_marques", id_marques);                         
            int idEquipement = Convert.ToInt32(ins1Comm.ExecuteScalar());
            if (idEquipement > 0) { errorSQL = true; } else { errorSQL = false; }

            for (int i = 0; i < lvCarac.Items.Count; i++)
            {
                // Ajout de nouvelles caractéristiques dans la table caracteristique        
                String valeur_composant = lvCarac.Items[i].SubItems[1].Text;
                String type_composant = lvCarac.Items[i].SubItems[3].Text;
                String unite_composant = lvCarac.Items[i].SubItems[4].Text;
                String ins2SQL = "INSERT INTO caracteristiques (valeur, id_unites_materiels, id_type_materiels) VALUES(@valeur_composant, @type_composant, @unite_composant) SELECT SCOPE_IDENTITY()";
                SqlCommand ins2Comm = new SqlCommand(ins2SQL, conn);
                ins2Comm.Parameters.AddWithValue("@valeur_composant", valeur_composant);
                ins2Comm.Parameters.AddWithValue("@type_composant", type_composant);
                ins2Comm.Parameters.AddWithValue("@unite_composant", unite_composant);                
                int idCaracteristique = Convert.ToInt32(ins2Comm.ExecuteScalar());
                if (idCaracteristique > 0) { errorSQL = true; } else { errorSQL = false; }

                // Gestion de la liaison entre la table caracteristiques et la table equipements  
                if (idEquipement > 0 && idCaracteristique > 0)
                {                    
                    String ins3SQL = "INSERT INTO caracteristique_equipement (id_equipements, id_caracteristiques) VALUES(@id_equipements, @id_caracteristiques)";
                    SqlCommand ins3Comm = new SqlCommand(ins3SQL, conn);
                    ins3Comm.Parameters.AddWithValue("@id_equipements", idEquipement.ToString());
                    ins3Comm.Parameters.AddWithValue("@id_caracteristiques", idCaracteristique.ToString());
                    int rowsAffected3 = ins3Comm.ExecuteNonQuery();
                    if (rowsAffected3 > 0) { errorSQL = true; } else { errorSQL = false; }
                }
            }

            // Gestion des logiciels de l'équipement
            if (idEquipement > 0)
            {
                for (int i = 0; i <= (clbLogiciels.Items.Count - 1); i++)
                {
                    if (clbLogiciels.GetItemChecked(i))
                    {                        
                        String ins4SQL = "INSERT INTO logiciel_equipement (id_equipements, id_logiciels) VALUES(@id_equipements, @id_logiciels)";
                        SqlCommand ins4Comm = new SqlCommand(ins4SQL, conn);
                        ins4Comm.Parameters.AddWithValue("@id_equipements", idEquipement.ToString());
                        ins4Comm.Parameters.AddWithValue("@id_logiciels", ((ComboBoxItem)clbLogiciels.Items[i]).HiddenValue.ToString());
                        int rowsAffected4 = ins4Comm.ExecuteNonQuery();
                        if (rowsAffected4 > 0) { errorSQL = true; } else { errorSQL = false; }
                    }
                }                
            }            

            // Fermeture de la connexion, confirmation, fermeture du formulaire d'ajout d'équipement et mise à jour du DataGridView des équipements
            if (errorSQL)
            {
                MessageBox.Show("Equipement ajouté", "L'équipement a été ajoutée avec succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                conn.Close();
                this.Hide();                
                UserSession User = new UserSession(idUser);
                GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
                //gp.Show();                
                gp.reloadData();
            }
            else
            {
                MessageBox.Show("Erreur lors de l'ajout de l'équipement", "L'équipement n'a pas pu être ajouté.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }          
        }

        /*
         *  Remplissage des combobox au chargement de la form
         */
        private void AjoutEquipement_Load(object sender, EventArgs e)
        {                           
            // Connection à la BDD
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();

            // Remplissage de la Combobox comboBox1 : Type de l'équipement
            String types_equipSQL = "SELECT id as id, libelle as libelle FROM types";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbTypeEquip.Items.Add(new ComboBoxItem(dr["libelle"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();

            // Remplissage de la Combobox comboBox3 : Etat de l'équipement
            String etats_equipSQL = "SELECT id as id, libelle as libelle FROM etats";
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    comboBox3.Items.Add(new ComboBoxItem(dr2["libelle"].ToString(), dr2["id"].ToString()));
                }
            }
            dr2.Close();

            // Remplissage de la Combobox comboBox3 : Marque de l'équipement
            String marques_equipSQL = "SELECT id as id, nom as nom FROM marques";
            SqlCommand marques_equipComm = new SqlCommand(marques_equipSQL, conn);
            SqlDataReader dr3 = marques_equipComm.ExecuteReader();
            if (dr3.HasRows)
            {
                while (dr3.Read())
                {
                    cbMarqueEquip.Items.Add(new ComboBoxItem(dr3["nom"].ToString(), dr3["id"].ToString()));
                }
            }
            dr3.Close();

            // Remplissage de la Combobox comboBox4 : Fournisseur de l'équipement
            String fournisseurs_equipSQL = "SELECT id as id, nom as nom FROM fournisseurs";
            SqlCommand fournisseurs_equipComm = new SqlCommand(fournisseurs_equipSQL, conn);
            SqlDataReader dr4 = fournisseurs_equipComm.ExecuteReader();
            if (dr4.HasRows)
            {
                while (dr4.Read())
                {
                    comboBox4.Items.Add(new ComboBoxItem(dr4["nom"].ToString(), dr4["id"].ToString()));
                }
            }
            dr4.Close();

            // Remplissage de la Combobox cbTypeComposant : Type de matériel de l'équipement
            String types_composantSQL = "SELECT id as id, libelle as libelle FROM type_materiels";
            SqlCommand types_composantComm = new SqlCommand(types_composantSQL, conn);
            SqlDataReader dr5 = types_composantComm.ExecuteReader();
            if (dr5.HasRows)
            {
                while (dr5.Read())
                {
                    cbTypeComposant.Items.Add(new ComboBoxItem(dr5["libelle"].ToString(), dr5["id"].ToString()));
                }
            }
            dr5.Close();

            // Remplissage de la Combobox cbTypeComposant : Unité du matériel de l'équipement
            String unite_composantSQL = "SELECT id as id, libelle as libelle FROM unites_materiels";
            SqlCommand unite_composantComm = new SqlCommand(unite_composantSQL, conn);
            SqlDataReader dr6 = unite_composantComm.ExecuteReader();
            if (dr6.HasRows)
            {
                while (dr6.Read())
                {
                    cbUniteComposant.Items.Add(new ComboBoxItem(dr6["libelle"].ToString(), dr6["id"].ToString()));
                }
            }
            dr6.Close();

            // Remplissage de la Combobox cbTypeComposant : Logiciels de l'équipement
            string logicielsSQL = "select id, nom+' '+version+' ('+editeur+')' as 'Caracteristique' from logiciels";
            SqlCommand logicielsComm = new SqlCommand(logicielsSQL, conn);
            SqlDataReader dr7 = logicielsComm.ExecuteReader();
            if (dr7.HasRows)
            {
                while (dr7.Read())
                {
                    clbLogiciels.Items.Add(new ComboBoxItem(dr7["Caracteristique"].ToString(), dr7["id"].ToString()));
                }
            }
            dr7.Close();
        }

        private void btnAddCarac_Click(object sender, EventArgs e)
        {            
            string[] row = { ((ComboBoxItem)cbTypeComposant.SelectedItem).ToString(), tbValeurComposant.Text.ToString(), ((ComboBoxItem)cbUniteComposant.SelectedItem).ToString(), ((ComboBoxItem)cbTypeComposant.SelectedItem).HiddenValue, ((ComboBoxItem)cbUniteComposant.SelectedItem).HiddenValue };            
            ListViewItem itm;	        
            itm = new ListViewItem(row);
            lvCarac.Items.Add(itm);
        }

        private void InitializeListView()
        {            
            ColumnHeader columnHeader1 = new ColumnHeader();
            columnHeader1.Text = "Composant";
            columnHeader1.TextAlign = HorizontalAlignment.Left;
            //columnHeader1.Width = 146;

            ColumnHeader columnHeader2 = new ColumnHeader();
            columnHeader2.Text = "Valeur";
            columnHeader2.TextAlign = HorizontalAlignment.Left;
            //columnHeader2.Width = 142;

            ColumnHeader columnHeader3 = new ColumnHeader();
            columnHeader3.Text = "Unité";
            columnHeader3.TextAlign = HorizontalAlignment.Left;
            //columnHeader3.Width = 142;

            ColumnHeader columnHeader4 = new ColumnHeader();
            columnHeader4.Text = "";
            columnHeader4.TextAlign = HorizontalAlignment.Left;
            columnHeader4.Width = 0;

            ColumnHeader columnHeader5 = new ColumnHeader();
            columnHeader5.Text = "";
            columnHeader5.TextAlign = HorizontalAlignment.Left;
            columnHeader5.Width = 0;

            this.lvCarac.Columns.Add(columnHeader1);
            this.lvCarac.Columns.Add(columnHeader2);
            this.lvCarac.Columns.Add(columnHeader3);
        }

        private void AjoutEquipement_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            //gp.Show();
        }

        private void tbValeurComposant_Click(object sender, EventArgs e)
        {
            tbValeurComposant.Text = "";
        }  

    }
}
