﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestGSB1
{
    public partial class ResolIncident : Form
    {
        //private String connectionString =
        //            @"Data Source=(LocalDB)\v11.0;AttachDbFilename='" + Application.StartupPath + "\\assistance_bdd.mdf';Integrated Security=True";
        private String connectionString =
                    @"Data Source=89.227.196.102,14335;Initial Catalog=assistance_bdd;Integrated Security=False;User ID=sa;Password=P@ssw0rd;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private String id_incident;
        private String idUser;

        public ResolIncident(String id_incident_to_modify, String id_utilisateur)
        {
            InitializeComponent();
            id_incident = id_incident_to_modify;
            idUser = id_utilisateur;
        }

        private void btnResolu_Click(object sender, EventArgs e)
        {
            String mailVisiteurIncident = "";
            String solutionIncident = "";
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String etats_equipSQL = "SELECT ut.adresse_mail as mail_visiteur, solution as solution FROM incidents i LEFT JOIN utilisateurs ut ON ut.id = i.id_utilisateurs WHERE i.id = " + id_incident;
            SqlCommand etats_equipComm = new SqlCommand(etats_equipSQL, conn);
            SqlDataReader dr2 = etats_equipComm.ExecuteReader();
            if (dr2.HasRows)
            {
                while (dr2.Read())
                {
                    mailVisiteurIncident = dr2["mail_visiteur"].ToString();                    
                }
            }
            solutionIncident = tbSolution.Text.ToString();
            dr2.Close();            

            this.Cursor = Cursors.WaitCursor;
            String msgMail = "";
            if (cbMailVisiteur.Checked)
            {
                MessageBox.Show("Envoi du mail de confirmation en cours", "Veuillez patienter...", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Cursor = Cursors.WaitCursor;

                SmtpClient clientSmtp = new SmtpClient();

                string strFrom = "dimitri.sandron@outlook.fr";  // Changer les infos ici
                string strTo = mailVisiteurIncident;
                string strSubject = "Assitance GSB - Ticket Incident Résolu";
                string strBody = "Votre incident concernant votre matériel a été pris en charge. Voici la solution : \n" + solutionIncident;
                MailMessage messageMail = new MailMessage(strFrom, strTo, strSubject, strBody);

                clientSmtp = new SmtpClient("smtp-mail.outlook.com", 587); /// (smtp.gmail.com,587) pour gmail
                clientSmtp.UseDefaultCredentials = false;
                System.Net.NetworkCredential objSMTPUserInfo = new System.Net.NetworkCredential("dimitri.sandron@outlook.fr", /*Mettre mot de passe ici*/"DIDOU22041996!!!");
                clientSmtp.Credentials = objSMTPUserInfo;
                clientSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                clientSmtp.EnableSsl = true;
                clientSmtp.Send(messageMail);                

                msgMail = " Le mail a été envoyé au visiteur";
            }            

            String id_technicien = ((ComboBoxItem)cbTechIncident.SelectedItem).HiddenValue;
            String solution = tbSolution.Text.ToString();

            String upd_affectSQL = "UPDATE incidents SET solution = @solution, id_utilisateurs_1 = @id_technicien, date_intervention = @date_intervention, id_statuts = @id_statuts WHERE id = @id_incident";
            SqlCommand upd_affectComm = new SqlCommand(upd_affectSQL, conn);
            upd_affectComm.Parameters.AddWithValue("@solution", solution);
            upd_affectComm.Parameters.AddWithValue("@id_technicien", id_technicien);
            upd_affectComm.Parameters.AddWithValue("@date_intervention", dtpDateInterv.Value);
            upd_affectComm.Parameters.AddWithValue("@id_incident", this.id_incident);
            upd_affectComm.Parameters.AddWithValue("@id_statuts", "2");
           
            int rowsAffected = upd_affectComm.ExecuteNonQuery();

            if (rowsAffected > 0)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("L'incident a été cloturé" + msgMail, "Incident résolu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                UserSession User = new UserSession(idUser);
                GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
                //gp.Show();
            }
        }

        private void ResolIncident_Load(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            String types_equipSQL = "SELECT id, nom+' '+prenom as nom_prenom FROM utilisateurs where id_profils = 2";
            SqlCommand types_equipComm = new SqlCommand(types_equipSQL, conn);
            SqlDataReader dr = types_equipComm.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    cbTechIncident.Items.Add(new ComboBoxItem(dr["nom_prenom"].ToString(), dr["id"].ToString()));
                }
            }
            dr.Close();
        }

        private void ResolIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserSession User = new UserSession(idUser);
            GestionParc gp = new GestionParc(User.IdUser, User.Firstname + " " + User.Lastname, User.Adresse_mail, User.IdProfil, User.Avatar);
            gp.Show();
        }
    }
}
