﻿namespace TestGSB1
{
    partial class AjoutEquipement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AjoutEquipement));
            this.cbTypeEquip = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbMarqueEquip = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.btnAddCarac = new System.Windows.Forms.Button();
            this.lvCarac = new System.Windows.Forms.ListView();
            this.tbValeurComposant = new System.Windows.Forms.TextBox();
            this.cbUniteComposant = new System.Windows.Forms.ComboBox();
            this.cbTypeComposant = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.clbLogiciels = new System.Windows.Forms.CheckedListBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbTypeEquip
            // 
            this.cbTypeEquip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeEquip.FormattingEnabled = true;
            this.cbTypeEquip.Location = new System.Drawing.Point(155, 81);
            this.cbTypeEquip.Name = "cbTypeEquip";
            this.cbTypeEquip.Size = new System.Drawing.Size(339, 24);
            this.cbTypeEquip.TabIndex = 0;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarMonthBackground = System.Drawing.SystemColors.InactiveCaption;
            this.dateTimePicker1.CustomFormat = "yyyy-MM-dd";
            this.dateTimePicker1.Location = new System.Drawing.Point(155, 158);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(339, 22);
            this.dateTimePicker1.TabIndex = 2;
            this.dateTimePicker1.Value = new System.DateTime(2016, 2, 18, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Marque";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date d\'achat";
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ajout d\'une";
            // 
            // cbMarqueEquip
            // 
            this.cbMarqueEquip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarqueEquip.FormattingEnabled = true;
            this.cbMarqueEquip.Location = new System.Drawing.Point(155, 121);
            this.cbMarqueEquip.Name = "cbMarqueEquip";
            this.cbMarqueEquip.Size = new System.Drawing.Size(339, 24);
            this.cbMarqueEquip.TabIndex = 7;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(155, 500);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(339, 24);
            this.comboBox3.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 503);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Etat";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.button1.Font = new System.Drawing.Font("Eras Medium ITC", 10.8F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(198, 639);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(134, 46);
            this.button1.TabIndex = 11;
            this.button1.Text = "Enregistrer";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 541);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "Garantie";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(155, 541);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(302, 22);
            this.numericUpDown1.TabIndex = 14;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(463, 541);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "ans";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 585);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 17);
            this.label8.TabIndex = 16;
            this.label8.Text = "Fournisseur";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(155, 582);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(339, 24);
            this.comboBox4.TabIndex = 17;
            // 
            // btnAddCarac
            // 
            this.btnAddCarac.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddCarac.BackgroundImage")));
            this.btnAddCarac.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddCarac.Location = new System.Drawing.Point(465, 196);
            this.btnAddCarac.Name = "btnAddCarac";
            this.btnAddCarac.Size = new System.Drawing.Size(29, 28);
            this.btnAddCarac.TabIndex = 18;
            this.btnAddCarac.UseVisualStyleBackColor = true;
            this.btnAddCarac.Click += new System.EventHandler(this.btnAddCarac_Click);
            // 
            // lvCarac
            // 
            this.lvCarac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lvCarac.FullRowSelect = true;
            this.lvCarac.GridLines = true;
            this.lvCarac.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvCarac.Location = new System.Drawing.Point(155, 233);
            this.lvCarac.Name = "lvCarac";
            this.lvCarac.Size = new System.Drawing.Size(339, 97);
            this.lvCarac.TabIndex = 19;
            this.lvCarac.UseCompatibleStateImageBehavior = false;
            this.lvCarac.View = System.Windows.Forms.View.Details;
            // 
            // tbValeurComposant
            // 
            this.tbValeurComposant.AccessibleDescription = "";
            this.tbValeurComposant.Location = new System.Drawing.Point(321, 199);
            this.tbValeurComposant.Multiline = true;
            this.tbValeurComposant.Name = "tbValeurComposant";
            this.tbValeurComposant.Size = new System.Drawing.Size(69, 24);
            this.tbValeurComposant.TabIndex = 23;
            this.tbValeurComposant.Text = "Valeur";
            this.tbValeurComposant.Click += new System.EventHandler(this.tbValeurComposant_Click);
            // 
            // cbUniteComposant
            // 
            this.cbUniteComposant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbUniteComposant.FormattingEnabled = true;
            this.cbUniteComposant.Location = new System.Drawing.Point(396, 199);
            this.cbUniteComposant.Name = "cbUniteComposant";
            this.cbUniteComposant.Size = new System.Drawing.Size(63, 24);
            this.cbUniteComposant.TabIndex = 22;
            // 
            // cbTypeComposant
            // 
            this.cbTypeComposant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTypeComposant.FormattingEnabled = true;
            this.cbTypeComposant.Location = new System.Drawing.Point(155, 199);
            this.cbTypeComposant.Name = "cbTypeComposant";
            this.cbTypeComposant.Size = new System.Drawing.Size(160, 24);
            this.cbTypeComposant.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "caractéristique";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 368);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 17);
            this.label10.TabIndex = 31;
            this.label10.Text = "logiciel";
            // 
            // label11
            // 
            this.label11.AllowDrop = true;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 351);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "Ajout d\'un";
            // 
            // clbLogiciels
            // 
            this.clbLogiciels.CheckOnClick = true;
            this.clbLogiciels.FormattingEnabled = true;
            this.clbLogiciels.Location = new System.Drawing.Point(155, 351);
            this.clbLogiciels.Name = "clbLogiciels";
            this.clbLogiciels.Size = new System.Drawing.Size(339, 140);
            this.clbLogiciels.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("High Tower Text", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(162, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(252, 28);
            this.label12.TabIndex = 19;
            this.label12.Text = "Ajouter un équipement";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label12);
            this.panel1.Location = new System.Drawing.Point(-26, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(588, 45);
            this.panel1.TabIndex = 37;
            // 
            // AjoutEquipement
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(532, 707);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.clbLogiciels);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbValeurComposant);
            this.Controls.Add(this.cbUniteComposant);
            this.Controls.Add(this.cbTypeComposant);
            this.Controls.Add(this.lvCarac);
            this.Controls.Add(this.btnAddCarac);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.cbMarqueEquip);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cbTypeEquip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AjoutEquipement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ajouter un équipement";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AjoutEquipement_FormClosed);
            this.Load += new System.EventHandler(this.AjoutEquipement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbTypeEquip;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbMarqueEquip;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.Button btnAddCarac;
        private System.Windows.Forms.ListView lvCarac;
        private System.Windows.Forms.TextBox tbValeurComposant;
        private System.Windows.Forms.ComboBox cbUniteComposant;
        private System.Windows.Forms.ComboBox cbTypeComposant;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckedListBox clbLogiciels;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
    }
}